    
    
    
    

    
        <div>
    
        <div id="throne-game-message">
            <h2>Game Update</h2>
            <p>Welcome to Age 72! Good luck everyone on your quest for domination! 
<br><br>

<a href="https://www.youtube.com/watch?v=5kn4cB3CwvI" target="blank">Affairs of the State</a> the Utopia-Game live twitch broadcast Episode 1 has completed.  Thank you everyone who attended and participated. 
<br><br>
You can now send intel to custom sites, read more <a href="https://tinyurl.com/yak9mqx8" target="blank">here</a>.</p>
        </div>
    

    
        <div id="throne-monarch-message">
            <h2>Royal commands</h2>
            
                <p>This war is a tough one, no slacking. We need the best out of all. Work as a team<br><br>TM wave on 20th, Attackers wave on 21st.<br><br>Orders on Discord.</p>
            
            
        </div>
    


    
<h2>The Province of Key Lime Pie (<a href="/wol/game/kingdom_details/6/17">6:17</a>)<br>June 8 of YR2 (next tick: 13 minutes)</h2>
<table class="two-column-stats">
    <tbody>
        <tr>
            <th>Race</th>
            <td>Orc</td>
            <th>Soldiers</th>
            <td>752</td>
        </tr>
        <tr>
            <th>Ruler</th>
            <td>Lord KLP the Blessed</td>
            <th>Goblins</th>
            <td>0</td>
        </tr>
        <tr>
            <th>Land</th>
            <td>618</td>
            <th>Trolls</th>
            <td>4,106</td>
        </tr>
        <tr>
            <th>Peasants</th>
            <td>6,272</td>
            <th>Ogres</th>
            <td>3,893</td>
        </tr>
        <tr>
            <th>Building Eff.</th>
            <td>91%</td>
            <th>Thieves</th>
            <td>
                
                    808 (48%)
                
            </td>
        </tr>
        <tr>
            <th>Money</th>
            <td>168</td>
            <th>Wizards</th>
            <td>
                
                    981 (76%)
                
            </td>
        </tr>
        <tr>
            <th>Food</th>
            <td>8,746</td>
            <th>War Horses</th>
            <td>4,596</td>
        </tr>
        <tr>
            <th>Runes</th>
            <td>390</td>
            <th>Prisoners</th>
            <td>0</td>
        </tr>
        <tr>
            <th>Trade Balance</th>
            <td>-67,234</td>
            <th>Off. Points</th>
            <td>70,718</td>
        </tr>
        <tr>
            <th>Networth</th>
            <td>123,656 gold coins</td>
            <th>Def. Points</th>
            <td>34,485</td>
        </tr>
    </tbody>
</table>


<p class="advice-message">Our Kingdom is at WAR!</p>

<p class="advice-message">We are covered by the Barrier ritual with 85.3% strength left!</p>



    

    <h2>Info</h2>
        <p>Spells:
        
                    

                            
                                <span class="good">
                            
                            Minor Protection
                            </span>
                        (10 days)
                    

                            
                                <span class="good">
                            
                            Greater Protection
                            </span>
                        (16 days)
                    

                            
                                <span class="good">
                            
                            Love and Peace
                            </span>
                        (8 days)
                    
        
        </p>

        
        <p>Armies : 
        
                
                    (0.07 days left) (38)
                
        
                
                    (0.10 days left) (32)
                
        
                
                    (0.18 days left) (22)
                
        
                
                    (0.21 days left) (12)
                
        
        </p>
        

    <h2>Recent News</h2>
    
        <p>
            No new happenings
        </p>
    

    <p><a href="/wol/game/province_news">See all news</a></p>
</div>
    
    
    
    
    
    
    

    <form>
    <div style="display:none"><input type="hidden" name="csrfmiddlewaretoken" value="AYfo6M3UcKzry0QZtFjfdCgIrsWSx0sl"></div>
    </form>

    ,Game Update
Welcome to Age 72! Good luck everyone on your quest for domination! 

Affairs of the State the Utopia-Game live twitch broadcast Episode 1 has completed. Thank you everyone who attended and participated. 

You can now send intel to custom sites, read more here.

Royal commands
This war is a tough one, no slacking. We need the best out of all. Work as a team

TM wave on 20th, Attackers wave on 21st.

Orders on Discord.

The Province of Key Lime Pie (6:17)
June 8 of YR2 (next tick: 13 minutes)
Race	Orc	Soldiers	752
Ruler	Lord KLP the Blessed	Goblins	0
Land	618	Trolls	4,106
Peasants	6,272	Ogres	3,893
Building Eff.	91%	Thieves	808 (48%)
Money	168	Wizards	981 (76%)
Food	8,746	War Horses	4,596
Runes	390	Prisoners	0
Trade Balance	-67,234	Off. Points	70,718
Networth	123,656 gold coins	Def. Points	34,485
Our Kingdom is at WAR!

We are covered by the Barrier ritual with 85.3% strength left!

Info
Spells: Minor Protection (10 days) Greater Protection (16 days) Love and Peace (8 days)

Armies : (0.07 days left) (38) (0.10 days left) (32) (0.18 days left) (22) (0.21 days left) (12)

Recent News
No new happenings

See all news

,http://utopia-game.com/wol/game/throne?mb,Key Lime Pie,testkey