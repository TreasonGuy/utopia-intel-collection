 <?php       $data_html = '
    
    
    
    

    
    
    
    
        <h2>Building Effectiveness</h2>
        <p>The structures we construct throughout our lands may or may not function at full capacity, depending on two major factors. First, people must work in the buildings to realize their full utility (each person is capable of performing 1.5 jobs). Second, money invested in tools sciences can raise efficiency as well. Note that changes in employment require time to shift your building efficiency. Listed below are the net effects of each of our buildings, not including personality or racial bonuses.</p>

        

<h2>Statistics</h2>
<table class="two-column-stats">
    <tbody>
        <tr>
            <th>Available Workers</th>
            <td>6,272</td>
            <th>Building Efficiency</th>
            <td>90.5%</td>
        </tr>
        <tr>
            <th>Available Jobs</th>
            <td>12,850</td>
            <th>Workers Needed for Max. Efficiency</th>
            <td>8,609</td>
        </tr>
    </tbody><tbody>
</tbody></table>

<h2>Building Effects</h2>
<p>You will find that as we build more of certain building types, many new structures will be less effective. The Next 1% in parentheses below refers to the benefits of dedicating 1% more of your land to a particular building type.</p>

<table id="council-internal-build-effects" class="data">
    <thead>
        <tr>
            <th>Building type</th>
            <th>Quantity</th>
            <th>% of Total</th>
            <th>Current Effects (effect of next 1%)</th>
        </tr>
    </thead>
    <tbody>
        
            <tr class="odd">
                <th>Barren Land</th>
                <td>43</td>
                <td>6.6%</td>
                <td>
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Homes</th>
                <td>88</td>
                <td>13.4%</td>
                <td>
                    
                    
                        Increase max population by 880<br>
                    
                        Increase birth rates by 42.05% (2.61%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Farms</th>
                <td>49</td>
                <td>7.5%</td>
                <td>
                    
                    
                        Produce 3,104 bushels per day<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Mills</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        Lower building costs by 0.0% (3.58%)<br>
                    
                        Reduce exploration costs in gold by 0.0% (2.69%)<br>
                    
                        Reduce exploration costs in men by 0.0% (1.79%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Banks</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        Produce 0 gold coins per day<br>
                    
                        0.0% higher income (1.12%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Training Grounds</th>
                <td>145</td>
                <td>22.1%</td>
                <td>
                    
                    
                        23.38% higher offensive efficiency (0.74%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Armouries</th>
                <td>103</td>
                <td>15.7%</td>
                <td>
                    
                    
                        17.97% lower military training costs (0.92%)<br>
                    
                        23.96% lower military wages &amp; draft costs (1.22%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Military Barracks</th>
                <td>86</td>
                <td>13.1%</td>
                <td>
                    
                    
                        15.46% lower attack times (0.99%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Forts</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        0.0% higher defensive efficiency (1.79%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Guard Stations</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        0.0% lower resource losses when attacked (1.79%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Hospitals</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        0.0% chance to cure the plague each day (1.79%)<br>
                    
                        0.0% lower military losses (2.69%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Guilds</th>
                <td>49</td>
                <td>7.5%</td>
                <td>
                    
                    
                        1.0 wizards trained per day<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Towers</th>
                <td>33</td>
                <td>5.0%</td>
                <td>
                    
                    
                        Produce 358 runes per day<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Thieves\' Dens</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        0.0% higher thievery effectiveness (2.69%)<br>
                    
                        0.0% lower losses in thievery operations (3.58%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Watch Towers</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        0.0% less damage caused by enemy thieves (2.69%)<br>
                    
                        0.0% chance of preventing enemy thief missions (1.79%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Laboratories</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        0.0% increase in new scientists (3.96%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Universities</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        Protect 0.0% of science from enemies (4.48%)<br>
                    
                    
                </td>
            </tr>
        
            <tr class="even">
                <th>Stables</th>
                <td>49</td>
                <td>7.5%</td>
                <td>
                    
                    
                        Hold 3,920 horses<br>
                    
                        Produce 88 horses per day<br>
                    
                    
                </td>
            </tr>
        
            <tr class="odd">
                <th>Dungeons</th>
                <td>0</td>
                <td>0.0%</td>
                <td>
                    
                    
                        House 0 prisoners<br>
                    
                    
                </td>
            </tr>
        
    </tbody>
</table>


<h2>Exploration/Construction Schedules</h2>
<table class="schedule">
    <thead>
        <tr>
            <th rowspan="2">Building type</th>
            <th colspan="24">Schedule (number of days)</th>
        </tr>
        <tr>
            
                <th>1</th>
            
                <th>2</th>
            
                <th>3</th>
            
                <th>4</th>
            
                <th>5</th>
            
                <th>6</th>
            
                <th>7</th>
            
                <th>8</th>
            
                <th>9</th>
            
                <th>10</th>
            
                <th>11</th>
            
                <th>12</th>
            
                <th>13</th>
            
                <th>14</th>
            
                <th>15</th>
            
                <th>16</th>
            
                <th>17</th>
            
                <th>18</th>
            
                <th>19</th>
            
                <th>20</th>
            
                <th>21</th>
            
                <th>22</th>
            
                <th>23</th>
            
                <th>24</th>
            
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Barren Land</th>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
        </tr>
        
        <tr>
            <th>Homes</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Farms</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Mills</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Banks</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Training Grounds</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Armouries</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Military Barracks</th>

            
                <td>11</td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Forts</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Guard Stations</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Hospitals</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Guilds</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Towers</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Thieves\' Dens</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Watch Towers</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Laboratories</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Universities</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Stables</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
        <tr>
            <th>Dungeons</th>

            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            
                <td></td>
            

        </tr>
        
    </tbody>
</table>


    
    
    
    

    <form>
    <div style="display:none"><input type="hidden" name="csrfmiddlewaretoken" value="AYfo6M3UcKzry0QZtFjfdCgIrsWSx0sl"></div>
    </form>

    ';
        $data_simple = "Building Effectiveness
The structures we construct throughout our lands may or may not function at full capacity, depending on two major factors. First, people must work in the buildings to realize their full utility (each person is capable of performing 1.5 jobs). Second, money invested in tools sciences can raise efficiency as well. Note that changes in employment require time to shift your building efficiency. Listed below are the net effects of each of our buildings, not including personality or racial bonuses.

Statistics
Available Workers	6,272	Building Efficiency	90.5%
Available Jobs	12,850	Workers Needed for Max. Efficiency	8,609
Building Effects
You will find that as we build more of certain building types, many new structures will be less effective. The Next 1% in parentheses below refers to the benefits of dedicating 1% more of your land to a particular building type.

Building type	Quantity	% of Total	Current Effects (effect of next 1%)
Barren Land	43	6.6%	
Homes	8,899	13.4%	Increase max population by 880
Increase birth rates by 42.05% (2.61%)
Farms	49	7.5%	Produce 3,104 bushels per day
Mills	0	0.0%	Lower building costs by 0.0% (3.58%)
Reduce exploration costs in gold by 0.0% (2.69%)
Reduce exploration costs in men by 0.0% (1.79%)
Banks	0	0.0%	Produce 0 gold coins per day
0.0% higher income (1.12%)
Training Grounds	145	22.1%	23.38% higher offensive efficiency (0.74%)
Armouries	103	15.7%	17.97% lower military training costs (0.92%)
23.96% lower military wages & draft costs (1.22%)
Military Barracks	86	13.1%	15.46% lower attack times (0.99%)
Forts	0	0.0%	0.0% higher defensive efficiency (1.79%)
Guard Stations	0	0.0%	0.0% lower resource losses when attacked (1.79%)
Hospitals	0	0.0%	0.0% chance to cure the plague each day (1.79%)
0.0% lower military losses (2.69%)
Guilds	49	7.5%	1.0 wizards trained per day
Towers	33	5.0%	Produce 358 runes per day
Thieves' Dens	0	0.0%	0.0% higher thievery effectiveness (2.69%)
0.0% lower losses in thievery operations (3.58%)
Watch Towers	0	0.0%	0.0% less damage caused by enemy thieves (2.69%)
0.0% chance of preventing enemy thief missions (1.79%)
Laboratories	0	0.0%	0.0% increase in new scientists (3.96%)
Universities	0	0.0%	Protect 0.0% of science from enemies (4.48%)
Stables 49  7.5%    Hold 3,920 horses
Produce 88 horses per day
Dungeons	0	0.0%	House 0 prisoners
Exploration/Construction Schedules
Building type	Schedule (number of days)
1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24
Barren Land																								
Homes																								
Farms																								
Mills																								
Banks																								
Training Grounds																								
Armouries																								
Military Barracks	11							1,121																
Forts																								
Guard Stations																								
Hospitals																								
Guilds																								
Towers																								
Thieves' Dens																								
Watch Towers																								
Laboratories																								
Universities																								
Dungeons																								
";

        $url = 'http://utopia-game.com/wol/game/council_internal';
        $province = 'Key Lime Pie';
        $key = 'testkey';
?>