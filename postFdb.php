<?php
	require('db_conn.php');
	$dbgstr = 'DEBUG: ';

	if(isset($_POST['action'])){
		if($_POST['action'] == 'feedback_issue'){

			$title = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
			$desc = filter_var($_POST['desc'], FILTER_SANITIZE_STRING);
			$type = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
			$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
			$author = filter_var($_POST['author'], FILTER_SANITIZE_STRING);

			$filterQuery = "INSERT INTO feedback(title, details, type, email, author) 
							VALUES ";

			if ($_POST['type'] == 'feedback') {
				$status = 'closed';
			}
			else{
				$status = 'open';
			}

			$filterQuery .= "('".$_POST['title']."','".$_POST['desc']."','".$_POST['type']."','".$_POST['email']."','".$_POST['author']."')";

			$result = mysqli_query($conn, $filterQuery);
			if($result){
				$dbgstr .= "POSTING: ".$result."<br/>";
			}
			else{
				$dbgstr .= "Error in Posting!<br/>";
			}

			$filterQuery = "SELECT id as LastID FROM feedback WHERE id = @@Identity";

			$response = array();
			$posts = array();
			$res = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($res)){
				$posts[] =	array(
								'id'		=>	$row['LastID'],
								'result'	=>	$result);
			}
		}
		else if($_POST['action'] == 'getTicket'){

			$Q_tkt = '';
			$Q_auth = '';
			$Q_title = '';
			$Q_stat = '';

			if ($_POST['type'] == 'tktid') {
				$Q_tkt = "id LIKE '%" . $_POST['search'] . "%'";
			}
			else if ($_POST['type'] == 'tktAuth') {
				$Q_auth = "author LIKE '%" . $_POST['search'] . "%'";
			}
			else if ($_POST['type'] == 'tktTag') {
				$Q_title = "title LIKE '%" . $_POST['search'] . "%'";
			}
			else if ($_POST['type'] == 'tktStat') {
				$Q_stat = "status LIKE '%" . $_POST['search'] . "%'";
			}

			$Q_filter = " WHERE ";
			$Q_filter .= ($Q_tkt != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_tkt
							:
								$Q_tkt
						:
							'';
			$Q_filter .= ($Q_auth != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_auth
							:
								$Q_auth
						:
							'';
			$Q_filter .= ($Q_title != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_title
							:
								$Q_title
						:
							'';
			$Q_filter .= ($Q_stat != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_stat
							:
								$Q_stat
						:
							'';

			$filterQuery = "SELECT id, title, details, author, status, originated 
							FROM feedback ";
			$filterQuery .= ($Q_filter != " WHERE ")?
								$Q_filter
							:
								'';

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'id'		=>	$row['id'],
								'title'		=>	$row['title'],
								'details'	=>	$row['details'],
								'author'	=>	$row['author'],
								'status'	=>	$row['status'],
								'origin'	=>	$row['originated']);
			}

		}
		$response['posts'] = $posts;
		$response['query'] = $filterQuery;
		$response['debugStr'] = $dbgstr;
		echo(json_encode($response));
	}

	$conn->close();
?>