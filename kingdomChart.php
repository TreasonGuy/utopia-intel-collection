<!DOCTYPE html>
<html>
	<head>
		<?php 
			require('template/head.php');
		?>
		<style type="text/css">

			body {
			  /* Margin bottom by footer height */
			  margin-bottom: 60px;
			  background: url('/Utopia/img/map1.jpeg') no-repeat center center fixed;
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
			  color:#fff;
			  background-color:#eee;
			  font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
			  overflow-y:scroll;
			}

			.navbar-nav > li{
				min-width: 90px;
				text-align: center;
			}

			#details{
				background-color: #666;
				border-radius: 10px;
				padding-top: 5px;
			}

			.btn-link, .btn-link:hover, .btn-link:active, .btn-link:focus{
				color: white;
				border: 1px solid white;
				border-radius: 4px;
			}

		</style>
	</head>

	<body id="homearea">
		<div class="showOnLoad" >
		
		<?php
			require('template/navigation.php');
		?>

		<section class="container-fluid" style="margin-top: 80px;">
			<article style="opacity: none;">
				<div class="col-sm-12" style="padding: 0;">
					<form id="chartSelect">
					<div class="form-group col-sm-5">
					  <label for="xaxis" style="color: #222;">Select axis:</label>
					  <select class="form-control graphTrigger" id="xaxis">
					    <option value="honor">Honor</option>
					    <option value="nw" selected="true">Networth</option>
					    <option value="land">Land</option>
					  </select>
					</div>
					<div class="form-group col-sm-5">
					  	<label for="location" style="color: #222;">Enter KD location:</label>
						<input class="form-control" id="location" type="text" name="location" 
						placeholder="KD location(optional)" maxlength="5" />
					</div>
					<div class="form-group col-sm-2">
						<input type="submit" id="goButton" class="btn btn-primary btn-block" style="margin-top: 24px" value="GO!" />
					</div>
					</form>
				</div>
				<div class="col-sm-12" id="charts" style="	margin: 10px 0 10px 0; 
															margin-bottom: 35px;
															background-color: rgba(200,200,200,0.8);
															border-radius:10px;
															display: none;">
					<div class="col-sm-12" id="KDdetails" style="height: 500px; 
																 background-color: transparent;
																 display: none;"></div>
					<div class="col-sm-12" id="PVdetails" style="height: 500px; 
																 background-color: transparent;
																 display: none;"></div>
				</div>
			</article>
		</section>

		<?php
			require('template/footer.php');
		?>
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/treemap.js"></script>
		<script src='/Utopia/js/kdChart.js'></script>

	</body>
</html>