<?php 
	class Test{
		public $exploreLast;
		public $exploreRank;
		public $land;

		public function __construct($arg){
			$this->exploreLast = $arg[0];
			$this->exploreRank = $arg[1];
			$this->land = $arg[2];
		}

		public function setExploreRank(Test $lastdata, int $elapsed=1)
		{
			if ($lastdata->land <= $this->land) {
				$rank = 0;
				$noreset = 1;
				$this->exploreLast = max(0, ($this->land - $lastdata->land));
				$exp = abs($lastdata->land - $this->land);
				$expdiff = abs($lastdata->exploreLast - $exp);
				$allowance = ceil(0.08	* abs($lastdata->exploreLast));
				echo "<br>>><b>This Explore:</b>".$this->exploreLast."<b>Prev Explore:</b>".$lastdata->exploreLast."<b> EXP:</b>".$exp."<b> ExpDiff:</b>".$expdiff."<b> Allowance:</b>".$allowance;

				if ($expdiff > $allowance and $lastdata->exploreLast == 0) {
					// $this->boostactivity($currentTick - 1)
					echo " <i>Hit 1</i>";
					$rank += 1;
				}elseif ($expdiff > $allowance and $this->exploreLast == 0) {
					echo " <i>Hit 2</i>";
					$rank -= min(max(round(exp(($lastdata->exploreRank-7)/6),1),0.2), ($lastdata->exploreRank/2));
				}elseif ($expdiff <= $allowance and $exp != 0) {
					echo " <i>Hit 3</i>";
					$rank += 1;
				}elseif ($expdiff <= $allowance and $exp == 0) {
					echo " <i>Hit 4</i>";
					$rank -= min(max(round(exp(($lastdata->exploreRank-7)/6),1),0.3), ($lastdata->exploreRank/2));
				}elseif($expdiff > $allowance){
					echo " <i>Hit 5</i>";
					$rank += 0;
				}
			}
			elseif($elapsed <=1){
				echo "<br>>><b>This Explore:</b>".$this->exploreLast."<b>Prev Explore:</b>".$lastdata->exploreLast;
				$rank = 0;
				$noreset = 1;
			}else{
				$noreset = 0;
			}
			$this->exploreRank = max(0, $lastdata->exploreRank + $rank) * $noreset;
		}

		public function display()
		{
			echo "<br><b>Land:</b>".$this->land;
			echo "<br><b>ExploreRank:</b>".$this->exploreRank;
			echo "<br><b>ExploreLast:</b>".$this->exploreLast;
		}
	}

	$testData = array(	0 => array(100,101,102,102,102,103,103),
						1 => array(100,100,120,142,161,181,181,181),
						2 => array(100,100,120,142,202,221,221,221),
						3 => array(100,100,120,142,202,291,291,291),
						3 => array(100,100,120,142,232,301,301,301),
						4 => array(110,110,200,110,110,150,150,150),
						5 => array(100,110,90,100,111,70,70,70),
						6 => array(100,101,101,101,101,102,102,102,102,103,103,103,103,103,103,103,103,103,103,103,103,103,103,103),
						6 => array(50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,125,130,135,140,145,150,155,160,165,
									166,168,169,171,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172),
						);

	foreach ($testData as $key => $value) {
		print_r($value);
		foreach ($value as $k => $v) {
			if ($k == 0) {	
				$test[$k] = new Test(array(0, 0, $v));
			} else{
				$test[$k] = new Test(array($v - $value[$k - 1], 0, $v));
				$test[$k]->setExploreRank($test[$k-1]);
			}
			$test[$k]->display();
			echo "<br>----";
		}
		echo "<br><br>";
	}
?>