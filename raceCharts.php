	<!DOCTYPE html>
	<html>
		<head>
			<?php 
				require('template/head.php');
			?>
			<style type="text/css">

				body {
				  /* Margin bottom by footer height */
				  margin-bottom: 60px;
				  background: url('/Utopia/img/plane.jpeg') no-repeat center center fixed;
				  -webkit-background-size: cover;
				  -moz-background-size: cover;
				  -o-background-size: cover;
				  background-size: cover;
				  color:#fff;
				  background-color:#eee;
				  font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
				  overflow-y:scroll;
				}

				.navbar-nav > li{
					min-width: 90px;
					text-align: center;
				}

				#details{
					background-color: #666;
					border-radius: 10px;
					padding-top: 5px;
				}

				.btn-link, .btn-link:hover, .btn-link:active, .btn-link:focus{
					color: white;
					border: 1px solid white;
					border-radius: 4px;
				}

			</style>
		</head>

		<body id="homearea">
			<div class="showOnLoad" >
			
			<?php
				require('template/navigation.php');
			?>

			<section class="container-fluid main">
				<article style="opacity: none;">
					<div class="col-sm-5" style="padding: 0;">
						<div id="raceDist" style="margin: 10px 0 10px 0; height: 300px;"></div>
					</div>
					<div class="col-sm-7" id="test" style="margin: 10px 0 10px 0; height: 300px;">
						<br/>
						<br/>
						<h6 style="color: pink;">Click on the adjacent pi chart after changing any drop down value</h6>
						<form>
							<div class="form-group">
							  <label for="graphtype">Select graph type:</label>
							  <select class="form-control graphTrigger" id="graphtype">
							    <option value="spline">Line</option>
							    <option value="column">Bar</option>
							    <option value="scatter">Scatter</option>
							  </select>
							</div>
							<div class="form-group">
							  <label for="xaxis">Select x-axis:</label>
							  <select class="form-control graphTrigger" id="xaxis">
							    <option value="kdnw">KD Networth</option>
							    <option value="kdhon">KD Honor</option>
							    <option value="kdland">KD Land</option>
							    <option value="kdww">KD War Win</option>
							  </select>
							</div>
						</form>
					</div>	
					<div class="col-sm-12" id="details" style="margin: 10px 0 10px 0; height: 300px;">
					</div>	
				</article>
			</section>

			<?php
				require('template/footer.php');
			?>
			</div>

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script src="http://code.highcharts.com/highcharts.js"></script>
			<script src="http://code.highcharts.com/modules/exporting.js"></script>

			<script type="text/javascript">
				var action = 'racechart';
				var url = "/Utopia/raceData.php";

				var posting = $.post( url, {action: action} );

				posting.done(function( data ) {
					response = jQuery.parseJSON(data);

					if (jQuery.isEmptyObject(response)) {
						table = "No data found!";
					}
					else{
						var sum = 0;
						$.each(response.posts, function(k,v){
							sum = sum + parseInt(response.posts[k].y);
						});
						$.each(response.posts, function(k,v){
							response.posts[k].x = response.posts[k].y;
							response.posts[k].y = response.posts[k].y / sum * 100;
						});
						//console.log(response.posts[0]);
						//console.log(sum);
					      // Radialize the colors
					      Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
					          return {
					              radialGradient: {
					                  cx: 0.5,
					                  cy: 0.3,
					                  r: 0.7
					              },
					              stops: [
					                  [0, color],
					                  [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
					              ]
					          };
					      });

					      // Build the chart
					      Highcharts.chart('raceDist', {
					      	  exporting: { enabled: false },
					          chart: {
					              plotBackgroundColor: null,
					              plotBorderWidth: null,
					              plotShadow: false,
					              backgroundColor:'transparent',
					              type: 'pie'
					          },
					          title: {
					              text: 'Utopia Race profile',
					              style: {
					              	color: 'white',
					              	fontFamily: 'sans-serif'
					              }
					          },
					          tooltip: {
					              pointFormat: '{series.name}: {point.x} <b>({point.percentage:.1f}%)</b>'
					          },
					          plotOptions: {
					              pie: {
					                  allowPointSelect: true,
					                  cursor: 'pointer',
					                  dataLabels: {
					                      enabled: true,
					                      format: '<b>{point.name}</b>',
					                      style: {
					                          color: '#DDD',
					                          textShadow: false,
					                          textOutline: false
					                      },
					                      connectorColor: 'white'
					                  }
					              }
					          },
					          series: [{
					              name: 'Race',
					              data: response.posts,
					              point: {
					              	events:{
					              		click: function(event){
					              			getRaceRatios(this.name);
					              		}
					              	}
					              }
					          }]
					      });
					  }
					});

				function getRaceRatios(race){
					var action = 'distribution';
					var axis = $('#xaxis').val();
					var type = $('#graphtype').val();
					//console.log(axis);
					var url = "/Utopia/raceData.php";

					var posting = $.post( url, {action	: action,
												axis 	: axis,
												race 	: race} );

					posting.done(function( data ) {
						error = 0;

						try{
							response = data;
							response = jQuery.parseJSON(data);
						}catch(e){
							$("#details").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
							console.log(e.message+data.substring(0,data.indexOf('{')));
							error = 1;
						}
						//console.log('here');
						if (jQuery.isEmptyObject(response) || error == 1) {
							table = "No data found!";
						}
						else{
							//console.log(response.posts[0]);
							//console.log(race);
							var data = [];
							$.each(response.posts, function(k,v){
								data.push([parseFloat(response.posts[k].name), parseFloat(response.posts[k].y)]);
							});
							//console.log(data[0]);
							Highcharts.chart('details', {
								exporting: { enabled: false },
								chart: {
								        plotBackgroundColor: null,
								        plotBorderWidth: null,
								        plotShadow: true,
								        backgroundColor: '#666',
								        type: type,
								        spacingBottom: 10,
						                spacingTop: 10,
						                marginLeft: 80,
						                spacingLeft: 30,
						                marginRight: 50,
								},
							    title: {
							        text: '\'% '+race+'zz in kingdom\' vs \''+$('#xaxis option:selected').html()+'\'',
							        style: {
							        	color: 'white',
							        	fontFamily: 'sans-serif'
							        }
							    },

							    yAxis: {
							        title: {
							            text: 'Ratio',
								        style: {
								        	color: 'white',
								        	fontFamily: 'sans-serif'
								        }
							        },
							        labels: {
							                    style: {
							                        color: 'orange'
							                    }
							                }
							    },

							    xAxis: {
							        title: {
							            text: $('#xaxis option:selected').html(),
								        style: {
								        	color: 'white',
								        	fontFamily: 'sans-serif'
								        }
									},
							        labels: {
							                    style: {
							                        color: 'red'
							                    }
							                }
							    },

							    label:{
							    	formatter: function () {
							    	        if (this.value) {
							    	            return '<span style="fill: white;">' + this.value + '</span>';
							    	        }
							    	    }
							    },

							    series: [{
							    	showInLegend: false, 
							    	name: race,
							        data: data,
							        color: '#6AF'
							    }]

							});
						}
					});
				};

				getRaceRatios('Avian','line');

			</script>

		</body>
	</html>