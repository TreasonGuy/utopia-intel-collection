<?php

// ==============================================
// *** Fetching data from the Utopis servers  ***
// *** UNCOMMENT WHEN TAKING DATA FROM SERVER ***
// ==============================================
$url = "http://utopia-game.com/wol/game/kingdoms_dump/?key=l1FdkNfdklAs";
$ch = curl_init();

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept: application/json', "Content-type: application/json"));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL,$url);

$output=curl_exec($ch);
$info = curl_getinfo($ch);

if ($output === false || $info['http_code'] != 200) {
  $output = "No cURL data returned for $url [". $info->http_code. "]";
  if (curl_error($ch))
    $output .= "\n". curl_error($ch);
  }
else {
  // 'OK' status;
  ;
}

curl_close($ch);

// =========================================
// *** Local file for Dev purpose        ***
// *** UNCOMMENT WHEN DEVELOPING LOCALLY ***
// =========================================
// $output = file_get_contents("tf.json");

$json = json_decode($output);
// echo json_encode($json,JSON_PRETTY_PRINT);


// ========================
// *** Connecting to DB ***
// ========================
require('db_conn.php');

// ============== SUCCESS COUNTER ==================
// *** Keeping count for successful transactions ***
// =================================================
$total = 0;
$success = 0;

// ============ RESOLVING DATES ===============
// *** Extracting minutes and converting to ***
// *** SQL as well as php friendly date     ***
// ============================================
$insertedOn = date(strtotime($json[0]));
$minutes = date('i',$insertedOn);
$insertedOn = date("Y-m-d H:i:s", $insertedOn);

// =========== ARCHIVING OLD DATA =============
// ** KD info will be updated in archive DB ***
// ** at the first update after every tick. ***
// ** This is for chart generation purposes ***
// ============================================
if ((integer)$minutes > 0 and (integer)$minutes < 10) {
	$result = mysqli_query($conn, "	INSERT INTO kingdomsarchive
									SELECT * FROM kingdoms");
	usleep(150000);
	if($result){
		echo "ARCHIVING: ".$result."<br/>";
	}
	else{
		echo "Error in Archiving!<br/>";
	}
}
else{
	echo "Skipping ARCHIVE!<br/>";
}

// ==================================================
// *** Flushing out old data from latest KD table ***
// ==================================================
$result = mysqli_query($conn, "DELETE FROM kingdoms");
if($result){
	echo "FLUSHING: ".$result."<br/>";
}
else{
	echo "Error while Flushing old data!<br/>";
}

// ==================================
// *** Updating latest KD info DB ***
// ==================================
$query = "	INSERT INTO kingdoms(loc,count,land,name,stance,warTarget,wars,honor,kdNetworth,warWin,updated)
			VALUES ";
/*
$duplicate = "	ON DUPLICATE KEY UPDATE loc='".$item->loc."',
				count='".$item->count."',
				land='".$item->land."',
				name='".$item->name."',
				stance='".$item->stance[0]."',
				warTarget='".$item->stance[1]."',
				wars='".$item->wars[0]."',
				honor='".$item->honor."',
				kdNetworth='".$item->nw."',
				warWin='".$item->wars[1]."',
				updated='".$insertedOn."'";
				*/

echo "<br/>Starting database population...<br/>";
foreach($json as $item) { 
	if(isset($item->loc))
	{
		if(is_array($item->stance)){
			$query 	= $query."('".$item->loc."',
						'".$item->count."',
						'".$item->land."',
						'".$item->name."',
						'".$item->stance[0]."',
						'".$item->stance[1]."',
						'".$item->wars[0]."',
						'".$item->honor."',
						'".$item->nw."',
						'".$item->wars[1]."',
						'".$insertedOn."'),";
		}
		else{
			$query 	= $query."('".$item->loc."',
						'".$item->count."',
						'".$item->land."',
						'".$item->name."',
						'".$item->stance."',
						'NULL',
						'".$item->wars[0]."',
						'".$item->honor."',
						'".$item->nw."',
						'".$item->wars[1]."',
						'".$insertedOn."'),";
		}
		$total += 1;
	}
        if(($total % 20) == 0){
                $query = substr($query, 0, -1).";";
                // echo "MYQUERY : <br/>".$query."<br/>";
                $return = mysqli_query($conn, $query);
                $query = "INSERT INTO kingdoms(loc,count,land,name,stance,warTarget,wars,honor,kdNetworth,warWin,updated)
			VALUES ";
        }
}
$query = substr($query, 0, -1).";";
echo "MYQUERY : <br/>".$query."<br/>";
$return = mysqli_query($conn, $query);
usleep(150000);
if($return){
	$success = mysqli_query($conn, "SELECT loc FROM kingdoms");
	$result = $success->num_rows;
}
echo "<br/>SUCCESS: ".$result."<br/>TOTAL: ".$total."<br/> <br/>"; 	

// ========== SUCCESS COUNTER ============
// *** Resetting counter for provinces ***
// =======================================
$total = 0;
$success = 0;

// ==================================================
// *** Flushing out old data from latest PV table ***
// ==================================================
$result = mysqli_query($conn, "DELETE FROM provinces");
if($result){
	echo "FLUSHING PV: ".$result."<br/>";
}
else{
	echo "Error while Flushing old province data!<br/>";
}

// ====================================
// *** Updating latest PV info DB   ***
// *** PV data needs to be inserted ***
// *** in segmants due to max limit ***
// *** on size of SQL query         ***
// ====================================
echo "<br/>Starting province database population...<br/>";
foreach($json as $superitem) { 
	if(isset($superitem->provinces[0]->loc)){
		$query = "	INSERT INTO provinces(loc,land,name,protected,race,online,honor,nw,updated)
					VALUES ";
		foreach($superitem->provinces as $item){
			if(isset($item->loc))
			{
				$query 	= $query."('".$item->loc."',
							'".$item->land."',
							'".$item->name."',
							'".$item->protected."',
							'".$item->race."',
							'".$item->online."',
							'".$item->honor."',
							'".$item->nw."',
							'".$insertedOn."'),";
				$total += 1;
				// echo "KD: ".$item->loc." NAME: ".$item->name;
			}
			else{
				echo "<br/> NOT INNER ISSET <br/>";
				var_dump($item);
			}
			// echo "<br/>";
		}
		$query = substr($query, 0, -1).";";
		// echo "MYQUERY : <br/>".$query."<br/>";
		$return = mysqli_query($conn, $query);
		usleep(100000);
		if($return){
			// echo "SUCCESS: ".$item->loc."<br/>";
		}
		else{
			echo "FAILED: ".$item->loc."<br/>";
		}
	}
	else{
		echo "<br/> NOT OUTER ISSET <br/>";
		var_dump($superitem);
	}
	// echo "<br/><br/>";
}
$success = mysqli_query($conn, "SELECT loc FROM provinces");
$result = $success->num_rows;
echo "<br/>SUCCESS: ".$result."<br/>TOTAL: ".$total; 

$conn->close();	

?>