<?php  
	/**
	* Class for a KD return object from the database
	*/
	class Empire
	{
		public Kingdom $kd
		public Province $pv[];
		public $land;
		public $name;
		public $stance;
		public $warTarget;
		public $wars;
		public $CFend;
		public $lastStance;
		public $lastStanceChange;
		public $honor;
		public $kdNetworth;
		public $warwin;
		public $age;
		private $ritual;

		private $avgHonor;
		private $avgNW;
		private $avgLand;

		public function __construct($arg)
		{
			$this->loc = $arg['loc'];
			$this->count = $arg['count'];
			$this->land = $arg['land'];
			$this->name = $arg['name'];
			$this->stance = $arg['stance'];
			$this->warTarget = $arg['warTarget'];
			$this->wars = $arg['wars'];
			$this->CFend = $arg['CFend'];
			$this->honor = $arg['honor'];
			$this->kdNetworth = $arg['kdNetworth'];
			$this->warwin = $arg['warWin'];
			$this->age = $arg['updated'];
			$this->lastStance = $arg['lastStance'];
			$this->lastStanceChange = $arg['lastStanceChange'];

			$this->avgHonor = $arg['honor'] / $arg['count'];
			$this->avgNW = $arg['kdNetworth'] / $arg['count'];
			$this->avgLand = $arg['land'] / $arg['count'];
		}

		public function display()
		{
			echo "<br>Location: ".$this->loc;
			echo "<br>Province count: ".$this->count;
			echo "<br>Total Honor: ".number_format($this->honor);
			echo "<br>Total NW: ".number_format($this->kdNetworth);
			echo "<br>Total Acres: ".number_format($this->land);
			echo "<br>Average Honor: ".number_format($this->avgHonor,2);
			echo "<br>Average NW: ".number_format($this->avgNW,2);
			echo "<br>Average Acres: ".number_format($this->avgLand,2);
			echo "<br>KD Name: ".$this->name;
			echo "<br>Current Stance: ".$this->stance;
			echo "<br>War Target: ".$this->warTarget;
			echo "<br>Total Wars: ".$this->wars;
			echo "<br>War Wins: ".$this->warwin;
			echo "<br>CF end time: ".$this->CFend;
			echo "<br>Last Updated: ".$this->age;
		}

		public function getMean()
		{
			return json_encode(	array(	'meanNW' 	=> $this->avgNW,
										'meanLand'	=> $this->avgLand,
										'meanHonor'	=> $this->avgHonor));
		}

		public function setRitual($value='NULL')
		{
			$this->ritual = $value;
		}
	}
?>