<?php 
	/**
	* Class for a KD return object from the database
	*/
	class Kingdom
	{
		public $loc;
		public $count;
		public $land;
		public $name;
		public $stance;
		public $warTarget;
		public $wars;
		public $honor;
		public $kdNetworth;
		public $warwin;
		public $age;

		public $stanceEnd;
		public $lastStance;
		public $lastStanceChange;
		private $avgHonor;
		private $avgNW;
		private $avgLand;

		public function __construct(){}

		public function dbPopulate($arg)
		{
			$this->loc = $arg['loc'];
			$this->count = $arg['count'];
			$this->land = $arg['land'];
			$this->name = $arg['name'];
			$this->stance = $arg['stance'];
			$this->warTarget = $arg['warTarget'];
			$this->wars = $arg['wars'];
			$this->stanceEnd = new DateTime($arg['stanceEnd']);
			$this->honor = $arg['honor'];
			$this->kdNetworth = $arg['kdNetworth'];
			$this->warwin = $arg['warWin'];
			$this->age = new DateTime($arg['updated']);
			$this->lastStance = $arg['lastStance'];
			$this->lastStanceChange = new DateTime($arg['lastStanceChange']);

			$this->avgHonor = $arg['honor'] / $arg['count'];
			$this->avgNW = $arg['kdNetworth'] / $arg['count'];
			$this->avgLand = $arg['land'] / $arg['count'];
		}

		public function jsonPopulate($arg, $updated)
		{
			//$arg = json_encode($arg);
			$this->loc = $arg->loc;
			$this->count = $arg->count;
			$this->land = $arg->land;
			$this->name = $arg->name;
			if (!is_array($arg->stance)) {
				$this->stance = $arg->stance;
			}else{
				$this->stance = $arg->stance[0];
				$this->warTarget = $arg->stance[1];
			}
			$this->wars = $arg->wars[1];
			$this->stanceEnd = new DateTime(START_OF_AGE_DATE);
			$this->honor = $arg->honor;
			$this->kdNetworth = $arg->nw;
			$this->warwin = $arg->wars[0];
			if (isset($updated)) {
				$this->age = $updated;
			}else{
				$this->age = new DateTime(START_OF_AGE_DATE);
			}
			$this->lastStance = '';
			$this->lastStanceChange = new DateTime(START_OF_AGE_DATE);

			$this->avgHonor = $arg->honor / $arg->count;
			$this->avgNW = $arg->nw / $arg->count;
			$this->avgLand = $arg->land / $arg->count;
		}

		public function display()
		{
			echo "<br>Location: ".$this->loc;
			echo "<br>Province count: ".$this->count;
			echo "<br>Total Honor: ".number_format($this->honor);
			echo "<br>Total NW: ".number_format($this->kdNetworth);
			echo "<br>Total Acres: ".number_format($this->land);
			echo "<br>Average Honor: ".number_format($this->avgHonor,2);
			echo "<br>Average NW: ".number_format($this->avgNW,2);
			echo "<br>Average Acres: ".number_format($this->avgLand,2);
			echo "<br>KD Name: ".$this->name;
			echo "<br>Current Stance: ".$this->stance;
			echo "<br>War Target: ".$this->warTarget;
			echo "<br>Total Wars: ".$this->wars;
			echo "<br>War Wins: ".$this->warwin;
			if (isset($this->stanceEnd)) {
				echo "<br>Stance end time: ".date_format($this->stanceEnd, "d/m/Y H:i:s");
			}
			if (isset($this->lastStanceChange)) {
				echo "<br>Stance change time: ".date_format($this->lastStanceChange, "d/m/Y H:i:s");
			}
			echo "<br>Last Updated: ".date_format($this->age, "d/m/Y H:i:s");
		}

		public function getMean()
		{
			return json_encode(	array(	'meanNW' 	=> $this->avgNW,
										'meanLand'	=> $this->avgLand,
										'meanHonor'	=> $this->avgHonor));
		}

		public function setStance(Kingdom $lasttick){
			$date = clone $this->age;
			if ($this->wars == ($lasttick->wars+1)) {
				$this->stance = 'eowcf';
				$this->lastStance = 'war';
				$this->lastStanceChange = clone $this->age;
				$this->stanceEnd = $date->modify('+3 days');
				$this->stanceEnd->sub(new DateInterval('PT'.$this->stanceEnd->format("i").'M'));
				echo "LOOP1 ".$this->loc."<br>";
			}elseif ($this->stance == 'war' and $lasttick->stance == 'eowcf' and $this->warTarget == $lasttick->warTarget) {
				$this->stance = 'eowcf';
				$this->lastStance = 'war';
				$this->lastStanceChange = $lasttick->lastStanceChange;
				$this->stanceEnd = $lasttick->stanceEnd;
				echo "LOOP2 ".$this->loc."<br>";
			}elseif ($this->stance != 'war' and $this->stance != 'eowcf' and $lasttick->stance == 'war') {
				$this->lastStance = 'eowcf';
				$this->lastStanceChange = $this->age;
				$this->stanceEnd = new DateTime(START_OF_AGE_DATE);
				echo "LOOP3 ".$this->loc."<br>";
			}
			elseif ($this->stance != $lasttick->stance) {
				$this->lastStanceChange = $this->age;
				$this->lastStance = $lasttick->stance;
				if (strtolower($this->stance) == 'fortified') {
					$this->stanceEnd = $date->modify('+3 days');
					$this->stanceEnd->sub(new DateInterval('PT'.$this->stanceEnd->format("i").'M'));
				}elseif (strtolower($this->stance == 'normal' and (strtolower($lasttick->stance) == 'aggressive' or strtolower($lasttick->stance) == 'fortified'))) {
					$this->stanceEnd = $date->modify('+4 days');
					$this->stanceEnd->sub(new DateInterval('PT'.$this->stanceEnd->format("i").'M'));
				}else{
					$this->stanceEnd = new DateTime(START_OF_AGE_DATE);
				}
				echo "LOOP4 ".$this->loc."<br>";
			}elseif ($lasttick->lastStance == '') {
				$this->lastStanceChange = new DateTime(START_OF_AGE_DATE);
				$this->lastStance = '';
				$this->stanceEnd = new DateTime(START_OF_AGE_DATE);
				//echo "LOOP5 ".$this->loc."<br>";
			}
			else{
				$this->lastStanceChange = $lasttick->lastStanceChange;
				$this->lastStance = $lasttick->lastStance;
				$this->stanceEnd = $lasttick->stanceEnd;
				echo "LOOP6 ".$this->loc."<br>";
			}
		}
	}
?>