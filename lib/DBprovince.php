<?php  
	/**
	* Class for a Province return object from the database
	* PRECAUTIONS : Always set in order exploreRank -> activity -> gbpRank
	*/
	class Province
	{
		public $loc;
		public $land;
		public $name;
		public $protected;
		public $race;
		public $online;
		public $honor;
		public $nw;
		public $age;

		private $gbpRank;
		private $activity;
		private $exploreRank;
		private $exploreLast;

		public function __construct($arg)
		{
			$this->loc = $arg['loc'];
			$this->land = $arg['land'];
			$this->name = $arg['name'];
			$this->protected = $arg['protected'];
			$this->race = $arg['race'];
			$this->online = $arg['online'];
			$this->honor = $arg['honor'];
			$this->nw = $arg['nw'];
			$this->age = $arg['updated'];

			if (!is_null($arg['gbp'])) {
				$this->gbpRank = $arg['gbp'];
			}else{$this->gbpRank = 0;}
			if (!is_null($arg['activity'])) {
				$this->activity = $arg['activity'];
			}else{$this->activity = array_fill(1,24,0);}
			if (!is_null($arg['explore'])) {
				$this->exploreRank = $arg['explore'];
			}else{$this->exploreRank = 0;}
			if (!is_null($arg['exploreLast'])) {
				$this->exploreLast = $arg['exploreLast'];
			}else{$this->exploreLast = 0;}
		}

		public function display()
		{
			echo "<br>Location: ".$this->loc;
			echo "<br>Province count: ".$this->count;
			echo "<br>Total Honor: ".number_format($this->honor);
			echo "<br>Total NW: ".number_format($this->kdNetworth);
			echo "<br>Total Acres: ".number_format($this->land);
			echo "<br>Average Honor: ".number_format($this->avgHonor,2);
			echo "<br>Average NW: ".number_format($this->avgNW,2);
			echo "<br>Average Acres: ".number_format($this->avgLand,2);
			echo "<br>KD Name: ".$this->name;
			echo "<br>Current Stance: ".$this->stance;
			echo "<br>War Target: ".$this->warTarget;
			echo "<br>Total Wars: ".$this->wars;
			echo "<br>War Wins: ".$this->warwin;
			echo "<br>CF end time: ".$this->CFend;
			echo "<br>Last Updated: ".$this->age;
		}

		public function setGBPrank(Province $lastdata, int $elapsed=1)
		{
			if ($lastdata->land > $this->land) {
				$gbp = round(( ($lastdata->land - $this->land)/$lastdata->land ) * 100 * 2.75);

				$decay = 0;
				while ($elapsed > 0) {
					if ($lastdata->gbpRank > 79) {
						$decay += 4;
					}elseif ($lastdata->gbpRank > 61) {
						$decay += 3;
					}elseif ($lastdata->gbpRank > 41) {
						$decay += 2;
					}elseif ($lastdata->gbpRank > 21) {
						$decay += 1;
					}elseif ($lastdata->gbpRank > 1) {
						$decay += 1;
					}else {
						$decay += 0;
					}
					$elapsed--;
				}
				$this->gbpRank = $lastdata->gbpRank + $gbp - $decay;
			}
		}

		public function setExploreRank(Test $lastdata, int $elapsed=1)
		{
			if ($lastdata->land <= $this->land) {
				$rank = 0;
				$noreset = 1;
				$this->exploreLast = max(0, ($this->land - $lastdata->land));
				$exp = abs($lastdata->land - $this->land);
				$expdiff = abs($lastdata->exploreLast - $exp);
				$allowance = ceil(0.08 * abs($lastdata->exploreLast));
				//echo "<br>>><b>This Explore:</b>".$this->exploreLast."<b>Prev Explore:</b>".$lastdata->exploreLast."<b> EXP:</b>".$exp."<b> ExpDiff:</b>".$expdiff."<b> Allowance:</b>".$allowance;

				if ($expdiff > $allowance and $lastdata->exploreLast == 0) {
					// $this->boostactivity($currentTick - 1)
					//echo " <i>Hit 1</i>";
					$rank += 1;
				}elseif ($expdiff > $allowance and $this->exploreLast == 0) {
					//echo " <i>Hit 2</i>";
					$rank -= min(max(round(exp(($lastdata->exploreRank-7)/6),1),0.2), ($lastdata->exploreRank/2));
				}elseif ($expdiff <= $allowance and $exp != 0) {
					//echo " <i>Hit 3</i>";
					$rank += 1;
				}elseif ($expdiff <= $allowance and $exp == 0) {
					//echo " <i>Hit 4</i>";
					$rank -= min(max(round(exp(($lastdata->exploreRank-7)/6),1),0.3), ($lastdata->exploreRank/2));
				}elseif($expdiff > $allowance){
					//echo " <i>Hit 5</i>";
					$rank += 0;
				}
			}
			elseif($elapsed <=1){
				//echo "<br>>><b>This Explore:</b>".$this->exploreLast."<b>Prev Explore:</b>".$lastdata->exploreLast;
				$rank = 0;
				$noreset = 1;
			}else{
				$noreset = 0;
			}
			$this->exploreRank = max(0, $lastdata->exploreRank + $rank) * $noreset;
		}

		public function setActivity(Province $lastdata, Kingdom $kddata)
		{
			$start = date_create(START_OF_AGE_DATE);
			$now = date_create($lastdata->age);
			$interval = date_diff($start,$now);
			$tick = $interval->format('%h') + 1;
			$yr = $interval->format('%d') / 7;

			$weights = 	0;
			if ($lastdata->nw > $this->nw and 
				abs($lastdata->land - $this->land and
				round($this->nw / $this->land) <= (200+23*log10(190*$yr+1)) { 			// Approx 	200 -> YR0
				switch ($kddata->stance) {												// 			266 -> YR4
					case 'fortified':													// 			278 -> YR12
						$weights += 4;
						break;
					case 'eowcf':
						$weights += 5;
						break;
					case 'aggressive':
						$weights += 1;
						break;
					case 'war':
						$weights += 0;
						break;
					case 'eowcf':
						$weights += 2;
						break;

					default:
						$weights += 0;
						break;
				}
			}
			if ($lastdata->exploreRank == 0 and $this->exploreRank > 0) {
				$this->activity[$tick] = 1;
			}


			for($i=1;$i<=24;$i++){
				
			}
		}

		private function boostActivity($tick)
		{
			$this->$activity[$tick] = $this->$activity[$tick] + 1;
		}
		
	}
?>