<?php
define('START_OF_AGE_DATE', "2017-01-1 18:00 GMT+00:00");
define('LOGFILE_LOCATION', "C:\\xampp\\htdocs\\Logs.txt");

$logs = "";
require_once("lib/DBkingdom.php");

// ==============================================
// *** Fetching data from the Utopia servers  ***
// *** UNCOMMENT WHEN TAKING DATA FROM SERVER ***
// ==============================================

$url = "http://utopia-game.com/wol/game/kingdoms_dump/?key=l1FdkNfdklAs";
$ch = curl_init();

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept: application/json', "Content-type: application/json"));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL,$url);

$output=curl_exec($ch);
$info = curl_getinfo($ch);

if ($output === false || $info['http_code'] != 200) {
  $output = "No cURL data returned for $url \[". $info['http_code'] . "]";
  if (curl_error($ch))
    $output .= "\n". curl_error($ch);
	$logs .= $output."\n";
  }
else {
  // 'OK' status;
  ;
}

curl_close($ch);

// =========================================
// *** Local file for Dev purpose        ***
// *** UNCOMMENT WHEN DEVELOPING LOCALLY ***
// =========================================
// $output = file_get_contents("tf.json");


// ====================================
// *** Populating the new classdata ***
// ====================================
$json = json_decode($output);
foreach ($json as $key => $value) {
	if (!isset($value->loc)) {
		$updated = new DateTime($value);
		echo "TIME:".date_format($updated,"Y-m-d H:i:s")."<br>";
	}else{
		$newKDdata = new Kingdom();
		$newKDdata->jsonPopulate($value, $updated);
		$newkdList[$newKDdata->loc] = new Kingdom();
		$newkdList[$newKDdata->loc] = $newKDdata;
	}
}

// ========================
// *** Connecting to DB ***
// ========================
require('db_conn.php');

// ============== SUCCESS COUNTER ==================
// *** Keeping count for successful transactions ***
// =================================================
$total = 0;
$success = 0;
$error = 0;

// ============ RESOLVING DATES ===============
// *** Extracting minutes and converting to ***
// *** SQL as well as php friendly date     ***
// ============================================
$insertedOn = date(strtotime($json[0]));
$minutes = date('i',$insertedOn);
$insertedOn = date("Y-m-d H:i:s", $insertedOn);

// =========== ARCHIVING OLD DATA =============
// ** KD info will be updated in archive DB ***
// ** at the first update after every tick. ***
// ** This is for chart generation purposes ***
// ============================================
$skipArchive = false;
if ((integer)$minutes > 0 and (integer)$minutes < 10 and !$skipArchive) {
	$result = mysqli_query($conn, "	INSERT INTO kingdomsarchive
									SELECT loc,land,stance,warTarget,honor,kdNetworth,warWin,updated FROM kingdoms");
	usleep(150000);
	if($result){
		echo "ARCHIVING at ".$minutes.": ".$result."<br/>";
		$logs .= "ARCHIVING at ".$minutes.": ".$result."\n";
	}
	else{
		echo "Error in Archiving!<br/>".mysqli_error($conn)."<br/>";
		$logs .= "Error in Archiving!\n".mysqli_error($conn)."\n";
	}
}
else{
	echo "Skipping ARCHIVE at ".$minutes."!<br/>";
	$logs .= "Skipping ARCHIVE at ".$minutes."!\n";
}

// =================================================
// *** Acquiring last tick data for calculations ***
// =================================================
$result = mysqli_query($conn, "SELECT * FROM kingdoms");
if($result){
	echo "ACQUIRING: 1<br/>";
	$logs .= "ACQUIRING: 1\n";
	$kdList[] = new Kingdom();	
	
	while($row=mysqli_fetch_array($result)){
		$kd = new Kingdom();
		$kd->dbPopulate($row);
		$kdList[$kd->loc] = $kd;
	}

	foreach ($kdList as $key => $obj) {
		if(isset($newkdList[$key])){
			$newkdList[$key]->setStance($kdList[$key]);
			//$newkdList[$key]->display();
			//echo "<br>";
		}
	}
}
else{
	echo "Error while ACQUIRING old data!<br/>";
	$logs .= "Error while ACQUIRING old data!\n";
}

// ==================================================
// *** Flushing out old data from latest KD table ***
// ==================================================
$result = mysqli_query($conn, "DELETE FROM kingdoms");
if($result){
	echo "FLUSHING: ".$result."<br/>";
	$logs .= "FLUSHING: ".$result."\n";
}
else{
	echo "Error while Flushing old data!<br/>";
	$logs .= "Error while Flushing old data!\n";
}

// ==================================
// *** Updating latest KD info DB ***
// ==================================
$query = "	INSERT INTO kingdoms(loc,count,land,name,stance,warTarget,wars,honor,kdNetworth,warWin,stanceEnd,lastStance,lastStanceChange,updated)
			VALUES ";
/*
$duplicate = "	ON DUPLICATE KEY UPDATE loc='".$item->loc."',
				count='".$item->count."',
				land='".$item->land."',
				name='".$item->name."',
				stance='".$item->stance[0]."',
				warTarget='".$item->stance[1]."',
				wars='".$item->wars[0]."',
				honor='".$item->honor."',
				kdNetworth='".$item->nw."',
				warWin='".$item->wars[1]."',
				updated='".$insertedOn."'";
				*/

echo "<br/>Starting database population...<br/>";
$logs .= "Starting database population...\n";
foreach($newkdList as $key => $item) { 
	if(isset($item->loc))
	{
		//$item->display();
		$query 	= $query."('".$item->loc."',
					'".$item->count."',
					'".$item->land."',
					'".$item->name."',
					'".$item->stance."',
					'".$item->warTarget."',
					'".$item->wars."',
					'".$item->honor."',
					'".$item->kdNetworth."',
					'".$item->warwin."',
					'".$item->stanceEnd->format("Y-m-d H:i:s")."',
					'".$item->lastStance."',
					'".$item->lastStanceChange->format("Y-m-d H:i:s")."',
					'".$item->age->format("Y-m-d H:i:s")."'),";
		$total += 1;
	}
}
$query = substr($query, 0, -1).";";
// echo "MYQUERY : <br/>".$query."<br/>";
$return = mysqli_query($conn, $query);
usleep(150000);
if($return){
	$success = mysqli_query($conn, "SELECT loc FROM kingdoms");
	$result = $success->num_rows;
}
if ($result != $total) {
	$error = 1;
        echo "ERROR : ".mysqli_error($conn)."<br/>";
        $logs .= "ERROR : ".mysqli_error($conn)."\n";
}
echo "<br/>====KD Db UPDATE STATUS ====<br/>SUCCESS: ".$result."<br/>TOTAL: ".$total." "; 	
echo "<br/>============================<br/> <br/>"; 
$logs .= "====KD Db UPDATE STATUS ====\nSUCCESS: ".$result."\nTOTAL: ".$total."\n============================\n";

// ========== SUCCESS COUNTER ============
// *** Resetting counter for provinces ***
// =======================================
$total = 0;
$success = 0;

// =================================================
// *** Acquiring last tick data for calculations ***
// =================================================
$result = mysqli_query($conn, "SELECT * FROM provinces");
if($result){
	echo "ACQUIRING PV: 1<br/>";
	$logs .= "ACQUIRING PV: 1\n";
	$kdList[] = new Kingdom();	
	
	while($row=mysqli_fetch_array($result)){
		$kd = new Kingdom();
		$kd->dbPopulate($row);
		$kdList[$kd->loc] = $kd;
	}

	foreach ($kdList as $key => $obj) {
		if(isset($newkdList[$key])){
			$newkdList[$key]->setStance($kdList[$key]);
			//$newkdList[$key]->display();
			//echo "<br>";
		}
	}
}
else{
	echo "Error while ACQUIRING old data!<br/>";
	$logs .= "Error while ACQUIRING old data!\n";
}

// ==================================================
// *** Flushing out old data from latest PV table ***
// ==================================================
$result = mysqli_query($conn, "DELETE FROM provinces");
usleep(10000);
if($result){
	echo "FLUSHING PV: ".$result."<br/>";
	$logs .= "FLUSHING PV: ".$result."\n";
}
else{
	echo "Error while Flushing old province data!<br/>";
	$logs .=  "Error while Flushing old province data!\n";
}

// ====================================
// *** Updating latest PV info DB   ***
// *** PV data needs to be inserted ***
// *** in segmants due to max limit ***
// *** on size of SQL query         ***
// ====================================
echo "<br/>Starting province database population...<br/>";
$logs .= "Starting province database population...\n";
foreach($json as $superitem) { 
	if(isset($superitem->provinces[0]->loc)){
		$query = "	INSERT INTO provinces(loc,land,name,protected,race,online,honor,nw,updated)
					VALUES ";
		foreach($superitem->provinces as $item){
			if(isset($item->loc))
			{
				switch ($item->honor) {
					case 'Knight':
					case 'Lady':
						$item->honor = 'Knight/Lady';
						break;
					case 'Lord':
					case 'Noble Lady':
						$item->honor = 'Lord/Noble Lady';
						break;
					case 'Baron':
					case 'Baroness':
						$item->honor = 'Baron/Baroness';
						break;
					case 'Viscount':
					case 'Viscountess':
						$item->honor = 'Viscount/Viscountess';
						break;
					case 'Prince':
					case 'Princess':
						$item->honor = 'Prince/Princess';
						break;
					case 'Count':
					case 'Countess':
						$item->honor = 'Count/Countess';
						break;
					case 'Marquis':
					case 'Marchioness':
						$item->honor = 'Marquis/Marchioness';
						break;
					case 'Duke':
					case 'Duchess':
						$item->honor = 'Duke/Duchess';
						break;
					case 'King':
					case 'Queen':
						$item->honor = 'King/Queen';
						break;
					default:
						$item->honor = 'Peasant';
						break;
				}
				$query 	= $query."('".$item->loc."',
							'".$item->land."',
							'".$item->name."',
							'".$item->protected."',
							'".$item->race."',
							'".false."',
							'".$item->honor."',
							'".$item->nw."',
							'".$insertedOn."'),";
				$total += 1;
				// echo "KD: ".$item->loc." NAME: ".$item->name;
			}
			else{
				echo "<br/> WARNING: Empty Province! <br/>";
				print_r($item);
				echo "<br/>";
				$logs .= "\nWARNING: Empty Province! \n".$item."\n";
			}
			// echo "<br/>";
		}
		$query = substr($query, 0, -1).";";
		// echo "MYQUERY : <br/>".$query."<br/>";
		$return = mysqli_query($conn, $query);
		usleep(100000);
		if($return){
			// echo "SUCCESS: ".$item->loc."<br/>";
		}
		else{
			echo "FAILED: ".$item->loc."<br/>";
			$logs .= "FAILED: ".$item->loc."\n";
			$error = 1;
			echo "ERROR : ".mysqli_error($conn)."<br/>";
			$logs .= "ERROR : ".mysqli_error($conn)."\n";
		}
	}
	else{
		echo "<br/> WARNING: No province data found! <br/>";
		$logs .= "WARNING: No province data found! \n".json_encode($superitem)."\n";
		print_r($superitem);
		echo "<br/>";
	}
	// echo "<br/><br/>";
}
$success = mysqli_query($conn, "SELECT loc FROM provinces");
$result = $success->num_rows;
echo "<br/>====PV Db UPDATE STATUS ====<br/>SUCCESS: ".$result."<br/>TOTAL: ".$total; 
echo "<br/>============================<br/> <br/>"; 
$logs .= "====PV Db UPDATE STATUS ====\nSUCCESS: ".$result."\nTOTAL: ".$total."\n============================\n";

/*if($error == 1){
	sleep(60);
	echo "<br/><b>ERRORS found on page. This connection will be reattempted in 60 seconds...</b>";
	header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . "/Utopia/loadDB.php");
}*/

$conn->close();	

// ERROR LOGGING
$logs .= "========= END OF LOGGING AT TIME ".$insertedOn." ==========\n\n";
// error_log("Testing error logging!", 0);
error_log($logs, 3, LOGFILE_LOCATION);
//mail("farawayland.utopia@gmail.com","My subject","Test mail","From: utopiamail@treasonguy.tk");
?>