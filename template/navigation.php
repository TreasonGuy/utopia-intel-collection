<?php

$active = 'class="active"';

$home = '';
$tf = '';
$kf = '';
$RC = '';
$HC = '';
$KC = '';
$wars = '';

switch ($_SERVER['REQUEST_URI']) {
	case '/Utopia/':
	case '/Utopia/index.php':
		$home = $active;
		break;

	case '/Utopia/tf.php':
		$tf = $active;
		break;
	case '/Utopia/kf.php':
		$kf = $active;
		break;
	case '/Utopia/raceCharts.php':
		$RC = $active;
		break;
	case '/Utopia/honorChart.php':
		$HC = $active;
		break;
	case '/Utopia/kingdomChart.php':
		$KC = $active;
		break;
	
	default:
		# code...
		break;
}

echo('
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigationx">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			</button>
			<a class="navbar-brand" href="#">U T O I N T E L</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li '.$home.'><a href="http://treasonguy.uphero.com/Utopia/">Home</a></li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Target Finder 
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li '.$tf.'><a href="http://treasonguy.uphero.com/Utopia/tf.php">Province</a></li>
						<li '.$kf.'><a href="http://treasonguy.uphero.com/Utopia/kf.php">Kingdom</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Tools 
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="http://utocalc.appspot.com/buildCalc" target="blank">Build Calculator</a></li>
						<li><a href="http://utocalc.appspot.com/draftCalc" target="blank">Draft Calculator</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Charts 
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li '.$RC.'><a href="http://treasonguy.uphero.com/Utopia/raceCharts.php">Racial Chart</a></li>
						<li '.$HC.'><a href="http://treasonguy.uphero.com/Utopia/honorChart.php">Honor Chart</a></li>
						<li '.$KC.'><a href="http://treasonguy.uphero.com/Utopia/kingdomChart.php">Kingdom Charts</a></li>
					</ul>
				</li>
				<li class="disabled '.$wars.'"><a href="#">Active Wars</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<button class="btn btn-danger navbar-btn" style="margin-right: 5px;">MyButton</button>
				</li>
			</ul>
		</div>
	</div>
</nav>
');

?>