<?php
echo '<div class="alerts">
		<div class="alert alert-danger alert-dismissable fade in" style="height: 100%; margin: 0px;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			This website is a work in progress. Due to the short timeline, some functionalities could not be implemented in time. <br/> These include: 
			<ul> 
			<li><b>PV finder EOWCF and GBP filters </b></li>
			<li><b>Active Wars page</b></li>
			</ul>
			<strong>Regret the inconvenience!</strong> <br/>
			However, some features on the website are intended to be a preview of what the final product will be at the time it goes live!
		</div><br/>
		<div class="alert alert-success alert-dismissable fade in" style="height: 100%; margin: 0px;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			The tables are sortable by clicking on headers <br/>
			Multiple (upto 4) columns can be sorted by holding shift while clicking the columns\' header. <br/>
			<strong>Click on filter button to be a control freak!</strong> 
		</div><br/>
		<div class="alert alert-danger alert-dismissable fade in" style="height: 100%; margin: 0px;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			This website is hosted on a free server, so the database can be slow to load.<br/>
			<strong>Thanks for the patience!</strong> <br/>
		</div><br/>
	</div>';
?>