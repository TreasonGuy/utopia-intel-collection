$(document).ready(function() {
    $('.showOnLoad').show();
});

$.tablesorter.addParser({
  id: "fancyNumber",
  is: function(s) {
    return /^[0-9]?[0-9,\.]*$/.test(s);
  },
  format: function(s) {
    return jQuery.tablesorter.formatFloat( s.replace(/,/g,'') );
  },
  type: "numeric"
});

$('.checkAllRace').click(function(){
	$('input[class="race"]').prop('checked', true);
});

$('.uncheckAllRace').click(function(){
	$('input[class="race"]').prop('checked', false);
});

$('.checkAllStance').click(function(){
	$('input[class="stance"]').prop('checked', true);
});

$('.uncheckAllStance').click(function(){
	$('input[class="stance"]').prop('checked', false);
});

$( "#provinceFinder" ).submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();
	$("#searchResult").empty().append("Loading...");

	var $form = $( this ),
	term = $form.find( "input[name='action']" ).val();
	kdNW = $form.find( "input[name='kdNW']" ).val();
	minNW = $form.find( "input[name='minNW']" ).val();
	maxNW = $form.find( "input[name='maxNW']" ).val();
	minAc = $form.find( "input[name='minAc']" ).val();
	maxAc = $form.find( "input[name='maxAc']" ).val();
	minHon = $form.find( "input[name='minHon']" ).val();
	maxHon = $form.find( "input[name='maxHon']" ).val();
	minkdnw = $form.find( "input[name='minkdnw']" ).val();
	maxkdnw = $form.find( "input[name='maxkdnw']" ).val();
	minIsl = $form.find( "input[name='minIsl']" ).val();
	maxIsl = $form.find( "input[name='maxIsl']" ).val();
	minPVnum = $form.find( "input[name='minPVnum']" ).val();
	maxPVnum = $form.find( "input[name='maxPVnum']" ).val();
	loc = $form.find( "input[name='loc']" ).val();
	name = $form.find( "input[name='kdname']" ).val();
	minWW = $form.find( "input[name='minWW']" ).val();
	maxWW = $form.find( "input[name='maxWW']" ).val();
	minWar = $form.find( "input[name='minWar']" ).val();
	maxWar = $form.find( "input[name='maxWar']" ).val();
	normal = $form.find( "input[name='normal']" ).prop('checked');
	war = $form.find( "input[name='war']" ).prop('checked');
	fort = $form.find( "input[name='fort']" ).prop('checked');
	eowcf = $form.find( "input[name='eowcf']" ).prop('checked');
	aggro = $form.find( "input[name='aggro']" ).prop('checked');
	minavian = $form.find( "input[name='minavian']" ).val();
	mindwarf = $form.find( "input[name='mindwarf']" ).val();
	minelf = $form.find( "input[name='minelf']" ).val();
	minfaery = $form.find( "input[name='minfaery']" ).val();
	minhalfer = $form.find( "input[name='minhalfer']" ).val();
	minhuman = $form.find( "input[name='minhuman']" ).val();
	minundead = $form.find( "input[name='minundead']" ).val();
	mindryad = $form.find( "input[name='mindryad']" ).val();
	minorc = $form.find( "input[name='minorc']" ).val();
	maxavian = $form.find( "input[name='maxavian']" ).val();
	maxdwarf = $form.find( "input[name='maxdwarf']" ).val();
	maxelf = $form.find( "input[name='maxelf']" ).val();
	maxfaery = $form.find( "input[name='maxfaery']" ).val();
	maxhalfer = $form.find( "input[name='maxhalfer']" ).val();
	maxhuman = $form.find( "input[name='maxhuman']" ).val();
	maxundead = $form.find( "input[name='maxundead']" ).val();
	maxdryad = $form.find( "input[name='maxdryad']" ).val();
	maxorc = $form.find( "input[name='maxorc']" ).val();
	stancefrom = $form.find( "select[id='stancefrom']" ).val();
	stanceto = $form.find( "select[id='stanceto']" ).val();
	hrAgo = $form.find( "input[name='hrAgo']" ).val();
	hrAhead = $form.find( "input[name='hrAhead']" ).val();
	cf = $form.find( "textarea[name='ceasefire']" ).val();
	url = "/Utopia/db2json.php";

	var posting = $.post( url, {action: term,
								kdNW: kdNW,
								minAc : minAc,
								maxAc : maxAc,
								minHon : minHon,
								maxHon : maxHon,
								minkdnw : minkdnw,
								maxkdnw : maxkdnw,
								minIsl : minIsl,
								maxIsl : maxIsl,
								minPVnum : minPVnum,
								maxPVnum : maxPVnum,
								loc : loc,
								kdname : name,
								minWW : minWW,
								maxWW : maxWW,
								minWar : minWar,
								maxWar : maxWar,
								normal : normal,
								war : war,
								fortified : fort,
								eowcf : eowcf,
								aggressive : aggro,
								minavian : minavian,
								mindwarf : mindwarf,
								minelf : minelf,
								minfaery : minfaery,
								minhalfling : minhalfer,
								minhuman : minhuman,
								minundead : minundead,
								mindryad : mindryad,
								minorc : minorc,
								maxavian : maxavian,
								maxdwarf : maxdwarf,
								maxelf : maxelf,
								maxfaery : maxfaery,
								maxhalfling : maxhalfer,
								maxhuman : maxhuman,
								maxundead : maxundead,
								maxdryad : maxdryad,
								maxorc : maxorc,
								stancefrom : stancefrom,
								stanceto : stanceto,
								hrAgo : hrAgo,
								hrAhead : hrAhead,
								ceasefire : cf,
								} );

	posting.done(function( data ) {
		error = 0;
				
		try{
			response = data;
			response = jQuery.parseJSON(data);
		}catch(e){
			$("#searchResult").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
			console.log(e.message+data);//.substring(0,data.indexOf('{')));
			error = 1;
		}
		console.log(response.queryExc);

		if (jQuery.isEmptyObject(response.posts) || error == 1) {
			table = "No data found!";
		}
		else{
			table = "<table id='resultTable' class='table table-hover'> \
					<thead> \
					<tr> \
					    <th>KD Name(Location)</th> \
					    <th>Land</th> \
					    <th>Wars</th> "+
					    // <th>Honor</th> \
					    "<th>Stance</th> \
					    <th>Networth</th> \
					    <th>Avg NWPA</th> \
					    <th>Prov. count</th> \
					</tr> \
					</thead> <tbody>";
			
			$.each(response.posts, function(k,v){
				switch(response.posts[k].stance.toLowerCase()){
					case 'eowcf' : response.posts[k].stance = 'EoWCF'; break;
					default : response.posts[k].stance = response.posts[k].stance.charAt(0).toUpperCase() + response.posts[k].stance.slice(1);
				}
				table = table + "<TR><TD>" + response.posts[k].name + "(" + response.posts[k].loc + ")";
				// "</TD><TD>" + response.posts[k].honor
				table = table + "</TD><TD>" + response.posts[k].land + "</TD><TD>" + response.posts[k].warWin + "/" +
						response.posts[k].wars + "</TD><TD>" + response.posts[k].stance + "</TD><TD>" + 
						response.posts[k].kdNetworth + "</TD><TD>" + response.posts[k].nwpa + "</TD><TD>" + 
						response.posts[k].count + "</TD></TR>" ;
			});
			table = table + "</tbody> </table>";
		}
		if(error == 0){
			$("#searchResult").empty().append( table );
			$("#searchResult").show();
			try{
	    		$("#resultTable").tablesorter({
	    										sortList: [[0,0]],
	    										headers: { 
	    										    1: { 
	    										        sorter:'fancyNumber' 
	    										    },
	    										    4: { 
	    										        sorter:'fancyNumber' 
	    										    } 
	    										} 
	    									});
	    	}catch(e){
	    		error = 1;
	    		$("#searchResult").empty().append('<h5 style="color:pink;">\
	    							<b>Error!</b> <br/>Something went terribly wrong.\
	    							<br/>If problem persists, please contact admin. \
	    							It would really help if you can paste the console log along. \
	    							<br/><br/><strong>Psst! The input field takes numbers only. \
	    							Try removing any extra characters.</strong>\
	    							</h5>');
	    		console.log(e);
	    	}
	    }
	});
});

