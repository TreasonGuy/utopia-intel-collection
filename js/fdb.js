// List of HTML entities for escaping.
var htmlEscapes = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#x27;',
  '/': '&#x2F;'
};

// Regex containing the keys listed immediately above.
var htmlEscaper = /[&<>"'\/]/g;

// Escape a string for HTML interpolation.
function escape(string) {
  return ('' + string).replace(htmlEscaper, function(match) {
    return htmlEscapes[match];
  });
};

$( "#submitFeedback" ).submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();
	$("#postResult").hide();
	$("#searchResult").show();
	$("#searchResult").empty().append("Submitting Feedback...");

	title = $( "#FDBtitle" ).val();
	title = escape(title);
	action = $( "#action" ).val();
	description = $( "#comment" ).val();
	description = escape(description);
	description = description.replace(/(?:\r\n|\r|\n)/g, '<br />');
	type = $( "#FDBtype" ).val();
	email = $( "#FDBemail" ).val();
	author = $( "#FDBauth" ).val();
	url = "/Utopia/postFdb.php";

	var posting = $.post( url, {title: title,
								desc: description,
								type: type,
								email: email,
								author: author,
								action: action
								});

	posting.done(function( data ) {
		error = 0;
						
		try{
			response = data;
			response = jQuery.parseJSON(data);
		}catch(e){
			$("#searchResult").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
			console.log(e.message+data.substring(0,data.indexOf('{')));
			error = 1;
		}
		
		$("#searchResult").empty().append("	<h4>Feedback submitted with ID : \
											<button class='btn btn-link' id='openTKT' onc \
											data-toggle='modal' \
											data-target='#tktModal' onclick=\"getTicket('TKT" + response.posts[0].id + "')\">TKT"+
											response.posts[0].id + "</button></h4>");
	});
});

$( "#ticketFinder" ).submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();
	$("#searchResult").show();
	$("#searchResult").empty().append("Searching Tickets...");

	var $form = $( this ),
	search = $form.find( "input[name='search']" ).val();
	type = $form.find( "#searchType" ).val();
	action = $form.find( "input[name='action']" ).val();
	url = "/Utopia/postFdb.php";

	var tkt = new RegExp('^TKT[0-9]*$');
	if(type == 'tktid' && tkt.test(search)){
		search = search.substr(3);
	}

	var posting = $.post( url, {
								search: search,
								type: type,
								action: action
								});

	posting.done(function( data ) {
		error = 0;
						
		try{
			response = data;
			response = jQuery.parseJSON(data);
		}catch(e){
			$("#searchResult").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
			console.log(e.message+data.substring(0,data.indexOf('{')));
			error = 1;
		}

		if (jQuery.isEmptyObject(response.posts) || error == 1) {
			table = "No data found!";
		}
		else{
			table = "<table id='resultTable' class='table table-hover'> \
					<thead> \
					<tr> \
					    <th>Ticket ID</th> \
					    <th>Title</th> \
					    <th>Author</th> \
					    <th>Status</th> \
					</tr> \
					</thead> <tbody>";
			$.each(response.posts, function(k,v){
				table = table + "<TR> <TD> <button class='btn btn-link' id='openTKT' onc \
											data-toggle='modal' \
											data-target='#tktModal' onclick=\"getTicket('TKT" + 
											response.posts[k].id + "')\">TKT"+
											response.posts[k].id + "</button>";
				
				$statString = '';
				switch(response.posts[k].status){
					case 'open': $statString = "<button type='button' class='btn btn-danger btn-xs'>Open</button>";
								 break;
					case 'closed': $statString = "<button type='button' class='btn btn-success btn-xs'>Closed</button>";
								 break;
					case 'cancelled': $statString = "<button type='button' class='btn btn-info btn-xs'>Cancelled</button>";
								 break;
					case 'postponed': $statString = "<button type='button' class='btn btn-warning btn-xs'>Postponed</button>";
								 break;
					case 'queried': $statString = "<button type='button' class='btn btn-default btn-xs'>Queried</button>";
								 break;
				}

				table = table + "</TD><TD>" + response.posts[k].title + 
								"</TD><TD>" + response.posts[k].author + 
								"</TD><TD>" + $statString + "</TD></TR>" ;
			});
			table = table + "</tbody> </table>";
		}

		if(error == 0){
			$("#searchResult").empty().append( table );
			$("#searchResult").show();
			try{
	    		$("#resultTable").tablesorter({sortList: [[0,0]]});
	    	}catch(e){
	    		error = 1;
	    		$("#searchResult").empty().append('<h5 style="color:pink;">\
	    							<b>Error!</b> <br/>Something went terribly wrong.\
	    							<br/>If problem persists, please contact admin. \
	    							It would really help if you can paste the console log along. \
	    							<br/><br/><strong>Psst! The input field takes numbers only. \
	    							Try removing any extra characters.</strong>\
	    							</h5>');
	    		console.log(e.message);
	    	}
	    }
	});
});

function getTicket(str) {

	tktNum = str.substr(3);
	type = "tktid";
	action = "getTicket";
	url = "/Utopia/postFdb.php";

	var posting = $.post( url, {
								search: tktNum,
								type: type,
								action: action
								});

	posting.done(function( data ) {
		error = 0;
						
		try{
			response = data;
			response = jQuery.parseJSON(data);
		}catch(e){
			$("#searchResult").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
			console.log(e.message+data.substring(0,data.indexOf('{')));
			error = 1;
		}
		
		$(".modal-title").empty().append("TKT" + response.posts[0].id + " : " + response.posts[0].title);
		$(".modal-body").empty().append(response.posts[0].details);
		$(".modal-auth").empty().append(response.posts[0].author);
		$(".modal-origin").empty().append(response.posts[0].origin);
		$statString = '';
		switch(response.posts[0].status){
			case 'open': $statString = "<button type='button' class='btn btn-danger btn-xs'>Open</button>";
						 break;
			case 'closed': $statString = "<button type='button' class='btn btn-success btn-xs'>Closed</button>";
						 break;
			case 'cancelled': $statString = "<button type='button' class='btn btn-info btn-xs'>Cancelled</button>";
						 break;
			case 'postponed': $statString = "<button type='button' class='btn btn-warning btn-xs'>Postponed</button>";
						 break;
			case 'queried': $statString = "<button type='button' class='btn btn-default btn-xs'>Queried</button>";
						 break;
		}
		$(".modal-status").empty().append($statString);
	});
};