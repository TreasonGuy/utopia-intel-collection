jQuery.tablesorter.addParser({
  id: "fancyNumber",
  is: function(s) {
    return /^[0-9]?[0-9,\.]*$/.test(s);
  },
  format: function(s) {
    return jQuery.tablesorter.formatFloat( s.replace(/,/g,'') );
  },
  type: "numeric"
});

$(document).ready(function() {
    $('.showOnLoad').show();
});

$('.checkAllRace').click(function(){
	$('input[class="race"]').prop('checked', true);
});

$('.uncheckAllRace').click(function(){
	$('input[class="race"]').prop('checked', false);
});

$('.checkAllStance').click(function(){
	$('input[class="stance"]').prop('checked', true);
});

$('.uncheckAllStance').click(function(){
	$('input[class="stance"]').prop('checked', false);
});

$( "#provinceFinder" ).submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();
	$("#searchResult").empty().append("Loading...");

	var $form = $( this ),
	term = $form.find( "input[name='action']" ).val();
	selfNW = $form.find( "input[name='selfNW']" ).val();
	selfAcre = $form.find( "input[name='selfAcre']" ).val();
	kdNW = $form.find( "input[name='kdNW']" ).val();
	minNW = $form.find( "input[name='minNW']" ).val();
	maxNW = $form.find( "input[name='maxNW']" ).val();
	minAc = $form.find( "input[name='minAc']" ).val();
	maxAc = $form.find( "input[name='maxAc']" ).val();
	minHon = $form.find( "select[name='minHon']" ).val();
	maxHon = $form.find( "select[name='maxHon']" ).val();
	minkdnw = $form.find( "input[name='minkdnw']" ).val();
	maxkdnw = $form.find( "input[name='maxkdnw']" ).val();
	minIsl = $form.find( "input[name='minIsl']" ).val();
	maxIsl = $form.find( "input[name='maxIsl']" ).val();
	minPVnum = $form.find( "input[name='minPVnum']" ).val();
	maxPVnum = $form.find( "input[name='maxPVnum']" ).val();
	loc = $form.find( "input[name='loc']" ).val();
	name = $form.find( "input[name='pvname']" ).val();
	minWW = $form.find( "input[name='minWW']" ).val();
	maxWW = $form.find( "input[name='maxWW']" ).val();
	normal = $form.find( "input[name='normal']" ).prop('checked');
	war = $form.find( "input[name='war']" ).prop('checked');
	fort = $form.find( "input[name='fort']" ).prop('checked');
	eowcf = $form.find( "input[name='eowcf']" ).prop('checked');
	aggro = $form.find( "input[name='aggro']" ).prop('checked');
	avian = $form.find( "input[name='avian']" ).prop('checked');
	dwarf = $form.find( "input[name='dwarf']" ).prop('checked');
	elf = $form.find( "input[name='elf']" ).prop('checked');
	faery = $form.find( "input[name='faery']" ).prop('checked');
	halfer = $form.find( "input[name='halfer']" ).prop('checked');
	human = $form.find( "input[name='human']" ).prop('checked');
	undead = $form.find( "input[name='undead']" ).prop('checked');
	dryad = $form.find( "input[name='dryad']" ).prop('checked');
	orc = $form.find( "input[name='orc']" ).prop('checked');
	prot = $form.find( "input[name='prot']" ).prop('checked');
	stancefrom = $form.find( "select[id='stancefrom']" ).val();
	stanceto = $form.find( "select[id='stanceto']" ).val();
	hrAgo = $form.find( "input[name='hrAgo']" ).val();
	hrAhead = $form.find( "input[name='hrAhead']" ).val();
	cf = $form.find( "textarea[name='ceasefire']" ).val();
	url = "/Utopia/db2json.php";

	var posting = $.post( url, {action: term,
								selfNW: selfNW,
								selfAcre: selfAcre,
								kdNW: kdNW,
								pvname: name,
								minNW : minNW,
								maxNW : maxNW,
								minAc : minAc,
								maxAc : maxAc,
								minHon : minHon,
								maxHon : maxHon,
								minkdnw : minkdnw,
								maxkdnw : maxkdnw,
								minIsl : minIsl,
								maxIsl : maxIsl,
								minPVnum : minPVnum,
								maxPVnum : maxPVnum,
								loc : loc,
								name : name,
								minWW : minWW,
								maxWW : maxWW,
								normal : normal,
								war : war,
								fortified : fort,
								eowcf : eowcf,
								aggressive : aggro,
								avian : avian,
								dwarf : dwarf,
								elf : elf,
								faery : faery,
								halfling : halfer,
								human : human,
								undead : undead,
								dryad : dryad,
								orc : orc,
								prot : prot,
								stancefrom : stancefrom,
								stanceto : stanceto,
								hrAgo : hrAgo,
								hrAhead : hrAhead,
								ceasefire : cf,
								} );

	posting.done(function( data ) {
		error = 0;
						
		try{
			response = data;
			response = jQuery.parseJSON(data);
		}catch(e){
			$("#searchResult").empty().append('<h5 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h5>');
			console.log(e.message+data);
			error = 1;
		}
		console.log(response.queryExc);
		console.log(response.debugStr);

		if (jQuery.isEmptyObject(response.posts) || error == 1) {
			table = "No data found!";
		}
		else{
			table = "<table id='resultTable' class='table table-hover'> \
					<thead> \
					<tr> \
					    <th>Province Name(Location)</th> \
					    <th>Land</th> \
					    <th>Race</th> "+
					    // <th>Honor</th> \
					    "<th>Stance</th> \
					    <th>Networth</th> \
					    <th>NWPA</th> \
					    <th>KD Networth</th> \
					</tr> \
					</thead> <tbody>";
					
			$.each(response.posts, function(k,v){
				switch(response.posts[k].stance.toLowerCase()){
					case 'eowcf' : response.posts[k].stance = 'EoWCF'; break;
					default : response.posts[k].stance = response.posts[k].stance.charAt(0).toUpperCase() + response.posts[k].stance.slice(1);
				}
				table = table + "<TR><TD>" + response.posts[k].name + "(" + response.posts[k].loc + ")";
				if(response.posts[k].protected == 1)
					table = table + "^";
				// "</TD><TD>" + response.posts[k].honor
				table = table + "</TD><TD>" + response.posts[k].land + "</TD><TD>" + response.posts[k].race + "</TD><TD>" + response.posts[k].stance + "</TD><TD>" + response.posts[k].nw + "</TD><TD>" 
						+ response.posts[k].nwpa + "</TD><TD>" + response.posts[k].kdNetworth + "</TD></TR>" ;
			});
			table = table + "</tbody> </table>";
		}
		if(error == 0){
			$("#searchResult").empty().append( table );
			$("#searchResult").show();
			try{
	    		$("#resultTable").tablesorter({
	    										sortList: [[0,0]],
	    										headers: { 
	    										    1: { 
	    										        sorter:'fancyNumber' 
	    										    },
	    										    4: { 
	    										        sorter:'fancyNumber' 
	    										    },
	    										    6: { 
	    										        sorter:'fancyNumber' 
	    										    }
	    										} 
	    									});
	    	}catch(e){
	    		error = 1;
	    		$("#searchResult").empty().append('<h5 style="color:pink;">\
	    							<b>Error!</b> <br/>Something went terribly wrong.\
	    							<br/>If problem persists, please contact admin. \
	    							It would really help if you can paste the console log along. \
	    							<br/><br/><strong>Psst! The input field takes numbers only. \
	    							Try removing any extra characters.</strong>\
	    							</h5>');
	    		console.log(e.message);
	    	}
	    }
	});
});