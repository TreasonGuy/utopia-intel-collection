$( "#chartSelect" ).submit(function( event ) {

	$("#charts").show();
	// Stop form from submitting normally
	event.preventDefault();

	var action = 'treemapKD';
	var xaxis = $('#xaxis').val();
	var loc = $('#location').val();
	var url = "/Utopia/raceData.php";

	if(loc){
		selectVal = 'provinces';
	}
	else{
		selectVal = 'kingdoms';
	}


	if(selectVal == 'kingdoms')
		$("#PVdetails").hide();
	else
		$("#KDdetails").hide();

	var posting = $.post( url, {action	: action,
								axis 	: xaxis,
								location: loc,
								select 	: selectVal} );

	posting.done(function( data ) {
		try{
			response = data;
			response = jQuery.parseJSON(data);
		}catch(e){
			$("#chart").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
			console.log(e.message+data);
			error = 1;
		}


		if (jQuery.isEmptyObject(response)) {
			table = "No data found!";
		}
		else{
			var data = [];
			$.each(response.posts, function(k,v){
				if (xaxis == 'honor' && selectVal == 'provinces') {
					data.push(
							{
							'name'	: response.posts[k].name,
							'info'	: response.posts[k].col,
							'color'	: Highcharts.getOptions().colors[Math.ceil(Math.random() * 10)],
							'value'	: response.posts[k].axis
							}
						);
				}
				else{data.push(
							{
							'name'	: response.posts[k].name,
							'info'	: response.posts[k].col,
							'color'	: Highcharts.getOptions().colors[Math.ceil(Math.random() * 10)],
							'value'	: parseInt(response.posts[k].axis)
							}
						);
				}
			});
			if(selectVal == 'provinces')
				if (xaxis == 'honor') 
					addPVhonorTreemap('PVdetails', data, loc);
				else
					addTreeMap('PVdetails', data, loc, xaxis, true);
			else
				addTreeMap('KDdetails', data, 'Utopia', xaxis, true, true, "Click for details!");
		}
	});
	if(selectVal == 'provinces')
		$("#PVdetails").show();
	else
		$("#KDdetails").show();
});

function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function addTreeMap(divID, data, loc, xaxis, showlabel, clickEvent, tooltipMsg){
	var axisName = {
				        'nw': 'Networth',
				        'honor': 'Honor',
				        'land': 'Land',
				        'race': 'Race'
				    };

	if(!tooltipMsg) tooltipMsg = '';
	try{
	Highcharts.chart(divID, {
		exporting: { enabled: false },
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        backgroundColor:'transparent'
	    },
	    colorAxis: {
	        minColor: '#FFFFFF',
	        maxColor: Highcharts.getOptions().colors[0]
	    },
	    series: [{
	        type: 'treemap',
	        dataLabels: {
	            enabled: showlabel,
	            style: {
	            	textShadow: false,
	            	textOutline: false
	            }
	        },
	        point: {
	        	events:{
	        		click: function(event){
	        			if(clickEvent){
		        			//if (xaxis == 'honor') 
		        			//	addPVhonorTreemap('PVdetails', data, loc);
		        			//else
		        				showProvinceMap(this.series.data[this.x].name, xaxis);
		        			
		        			$("#PVdetails").show();
		        		}
	        		}
	        	}
	        },
	        layoutAlgorithm: 'squarified',
	        data: data
	    }],
	    title: {
	        text: axisName[xaxis] + ' distribution in ' + loc,
	        style: {
	        	fontFamily: 'sans-serif',
	        	fontWeight: 'bold'
	        }
	    },
        tooltip: {
            formatter: function() {

			                return  '<b>' + this.series.data[this.x].name + '</b> [' +
			                		'<i>' + this.series.data[this.x].info + '</i> ]<br/>' +
			                		'<i>' + addCommas(this.series.data[this.x].value) + '</i><br/>' +
			                		'<i>' + tooltipMsg + '</i>';
			            }
        }
	});
	}
	catch(e){
		$("#chart").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
		console.log(e.message+data);
		error = 1;
	}	
	console.log('Successfully rendered chart.');
}

function showProvinceMap(loc, type){
	var url = "/Utopia/raceData.php";

	var posting = $.post( url, {select	: 'provinces',
								axis 	: type,
								location: loc,
								action 	: 'treemapKD'} );

	//console.log(loc);

	posting.done(function( data ) {
		try{
			response = data;
			response = jQuery.parseJSON(data);
		}catch(e){
			$("#PVdetails").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
			console.log(e.message+data);
			error = 1;
		}

		if (jQuery.isEmptyObject(response)) {
			table = "No data found!";
		}
		else{
			var data = [];
			$.each(response.posts, function(k,v){
				if (type == 'honor') {
					data.push(
							{
							'name'	: response.posts[k].name,
							'info'	: response.posts[k].col,
							'color'	: Highcharts.getOptions().colors[Math.ceil(Math.random() * 10)],
							'value'	: response.posts[k].axis
							}
						);
				}
				else{data.push(
							{
							'name'	: response.posts[k].name,
							'info'	: response.posts[k].col,
							'color'	: Highcharts.getOptions().colors[Math.ceil(Math.random() * 10)],
							'value'	: parseInt(response.posts[k].axis)
							}
						);
				}
			});

			if (type == 'honor') {
				addPVhonorTreemap('PVdetails', data, loc);}
			else
				addTreeMap('PVdetails', data, loc, type, true);
		}
	});
}

function addPVhonorTreemap(divID, data, loc){
	honordata = [];
	parent = {
		'Peasant'				: { 'color' : '#f15c80', 'value' : 0, 'id' : 'Peasant' },
		'Knight/Lady'			: { 'color' : '#f7a35c', 'value' : 1, 'id' : 'Knight/Lady' },
		'Lord/Noble Lady'		: { 'color' : '#f45b5b', 'value' : 2, 'id' : 'Lord/Noble Lady' },
		'Baron/Baroness'		: { 'color' : '#2b908f', 'value' : 3, 'id' : 'Baron/Baroness' },
		'Viscount/Viscountess'	: { 'color' : '#8085e9', 'value' : 4, 'id' : 'Viscount/Viscountess' },	
		'Count/Countess'		: { 'color' : '#e4d354', 'value' : 5, 'id' : 'Count/Countess' },
		'Marquis/Marchioness'	: { 'color' : '#91e8e1', 'value' : 6, 'id' : 'Marquis/Marchioness' },	
		'Duke/Duchess'			: { 'color' : '#f15c80', 'value' : 7, 'id' : 'Duke/Duchess' },
		'Prince/Princess'		: { 'color' : '#434348', 'value' : 8, 'id' : 'Prince/Princess' },
		'King/Queen'			: { 'color' : '#90ed7d', 'value' : 9, 'id' : 'King/Queen' }
	};

	$.each(data, function(k,v){
		honordata.push({
			'name' 	: v.name,
			'info' 	: v.info,
			'parent': parent[v.value]['id'],
			'value'	: parent[v.value]['value']
		});
	});
	$.each(parent, function(k,v){
		honordata.push({
			'id'	: parent[k]['id'],
			'name'	: k,
			'color'	: parent[k]['color']
		})
	})

	Highcharts.chart(divID, {
		exporting: { enabled: false },
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        backgroundColor:'transparent'
	    },
	    series: [{
	        type: "treemap",
	        layoutAlgorithm: 'stripes',
	        alternateStartingDirection: true,
	        levels: [{
	            level: 1,
	            layoutAlgorithm: 'sliceAndDice',
	            dataLabels: {
	                enabled: true,
	                align: 'left',
	                verticalAlign: 'top',
	                style: {
		            	textShadow: false,
		            	textOutline: false,
	                    fontSize: '15px',
	                    fontWeight: 'bold'
	                }
	            }
	        },{
	            level: 2,
	            dataLabels: {
	                enabled: true,
	                style: {
		            	textShadow: false,
		            	textOutline: false,
	                }
	            }
	        }],
	        data: honordata
	    }],
	    title: {
	        text: 'Honor distribution in ' + loc,
	        style: {
	        	fontFamily: 'sans-serif',
	        	fontWeight: 'bold'
	        }
	    },
        tooltip: {
            formatter: function() {
			                return  '<b>' + this.series.data[this.x].name + '</b> [' +
			                		'<i>' + this.series.data[this.x].info + '</i> ]<br/>' +
			                		'<i>' + this.series.data[this.x].parent + '</i><br/>';
			            }
        }
	});
}