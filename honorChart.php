	<!DOCTYPE html>
	<html>
		<head>
			<?php 
				require('template/head.php');
			?>
			<style type="text/css">

				body {
				  /* Margin bottom by footer height */
				  margin-bottom: 60px;
				  background: url('/Utopia/img/chart.jpeg') no-repeat center center fixed;
				  -webkit-background-size: cover;
				  -moz-background-size: cover;
				  -o-background-size: cover;
				  background-size: cover;
				  color:#fff;
				  background-color:#eee;
				  font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
				  overflow-y:scroll;
				}

				.navbar-nav > li{
					min-width: 90px;
					text-align: center;
				}

				#details{
					background-color: #666;
					border-radius: 10px;
					padding-top: 5px;
				}

				.btn-link, .btn-link:hover, .btn-link:active, .btn-link:focus{
					color: white;
					border: 1px solid white;
					border-radius: 4px;
				}

			</style>
		</head>

		<body id="homearea">
			<div class="showOnLoad" >
			
			<?php
				require('template/navigation.php');
			?>

			<section class="container-fluid main">
				<article style="opacity: none;">
					<div class="col-sm-12" style="padding: 0;">
						<div id="raceDist" style="margin: 10px 0 10px 0; height: 300px;"></div>
					</div>
					<div class="col-sm-12" id="details" style="margin: 10px 0 10px 0; height: 300px;">
					</div>	
				</article>
			</section>

			<?php
				require('template/footer.php');
			?>
			</div>

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script src="http://code.highcharts.com/highcharts.js"></script>
			<script src="http://code.highcharts.com/modules/exporting.js"></script>

			<script type="text/javascript">
				var action = 'honorchart';
				var url = "/Utopia/raceData.php";

				var posting = $.post( url, {action: action} );

				posting.done(function( data ) {
					response = jQuery.parseJSON(data);

					if (jQuery.isEmptyObject(response)) {
						table = "No data found!";
					}
					else{
						var sum = 0;
						$.each(response.posts, function(k,v){
							sum = sum + parseInt(response.posts[k].y);
						});
						$.each(response.posts, function(k,v){
							response.posts[k].x = response.posts[k].y;
							response.posts[k].y = response.posts[k].y / sum * 100;
						});
						// console.log(response.posts[0]);
						// console.log(sum);
					      // Radialize the colors
					      Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
					          return {
					              radialGradient: {
					                  cx: 0.5,
					                  cy: 0.3,
					                  r: 0.7
					              },
					              stops: [
					                  [0, color],
					                  [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
					              ]
					          };
					      });

					      // Build the chart
					      Highcharts.chart('raceDist', {
					      	  exporting: { enabled: false },
					          chart: {
					              plotBackgroundColor: null,
					              plotBorderWidth: null,
					              plotShadow: false,
					              backgroundColor:'transparent',
					              type: 'pie'
					          },
					          title: {
					              text: 'Utopia Honor profile',
					              style: {
					              	color: 'white',
					              	fontFamily: 'sans-serif'
					              }
					          },
					          tooltip: {
					              pointFormat: '{series.name}: {point.x} <b>({point.percentage:.1f}%)</b>'
					          },
					          plotOptions: {
					              pie: {
					                  allowPointSelect: true,
					                  cursor: 'pointer',
					                  dataLabels: {
					                      enabled: true,
					                      format: '<b>{point.name}</b>',
					                      style: {
					                          color: '#DDD',
					                          textShadow: false,
					                          textOutline: false
					                      },
					                      connectorColor: 'white'
					                  }
					              }
					          },
					          series: [{
					              name: 'Count',
					              data: response.posts,
					              point: {
					              	events:{
					              		click: function(event){
					              			getRaceRatios(this.name);
					              		}
					              	}
					              }
					          }]
					      });
					  }
					});

				function getRaceRatios(honor){
					var action = 'honordist';
					//console.log(axis);
					var url = "/Utopia/raceData.php";

					var posting = $.post( url, {action	: action,
												honor 	: honor} );

					posting.done(function( data ) {
						error = 0;

						try{
							response = data;
							response = jQuery.parseJSON(data);
						}catch(e){
							$("#details").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
							console.log(e.message+data.substring(0,data.indexOf('{')));
							error = 1;
						}
						//console.log('here');
						if (jQuery.isEmptyObject(response) || error == 1) {
							table = "No data found!";
						}
						else{
							//console.log(response.posts[0]);
							//console.log(race);
							var data = [];
							$.each(response.posts, function(k,v){
								data.push([response.posts[k].name, parseFloat(response.posts[k].y), parseFloat(response.posts[k].x), parseFloat(response.posts[k].total)]);
							});
							// console.log(data[0]);
							Highcharts.chart('details', {
								exporting: { enabled: false },
								chart: {
								        plotBackgroundColor: null,
								        plotBorderWidth: null,
								        plotShadow: true,
								        backgroundColor: '#666',
								        type: 'column',
								        spacingBottom: 10,
						                spacingTop: 10,
						                marginLeft: 80,
						                spacingLeft: 30,
						                marginRight: 50,
								},
							    title: {
							        text: honor+' per Race',
							        style: {
							        	color: 'white',
							        	fontFamily: 'sans-serif'
							        }
							    },
							    tooltip: {
							        formatter: function() {
							                return '<i>' + data[this.x][0] + '</i><br/>Count: <b>' + data[this.x][2] + '</b> of <b>' + data[this.x][3] ;
							            }
							    },
							    yAxis: {
							        title: {
							            text: 'Ratio',
								        style: {
								        	color: 'white',
								        	fontFamily: 'sans-serif'
								        }
							        },
							        labels: {
							                    style: {
							                        color: 'orange'
							                    }
							                }
							    },

							    xAxis: {
							        title: {
							            text: 'Race',
								        style: {
								        	color: 'white',
								        	fontFamily: 'sans-serif'
								        }
									},
							        labels: {
							        	formatter: function() { return data[this.value][0];},
					                    style: {
					                        color: 'red'
					                    }
					                }
							    },

							    label:{
							    	formatter: function () {
							    	        if (this.value) {
							    	            return '<span style="fill: white;">' + this.value + '</span>';
							    	        }
							    	    }
							    },

							    series: [{
							    	showInLegend: false, 
							    	name: honor,
							        data: data,
							        color: '#6AF'
							    }]

							});
						}
					});
				};

				getRaceRatios('Lord/Noble Lady');

			</script>

		</body>
	</html>