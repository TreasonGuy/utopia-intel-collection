	<!DOCTYPE html>
	<html>
		<head>
			<?php 
				require('template/head.php');
			?>
			<style type="text/css">

				body {
				  /* Margin bottom by footer height */
				  margin-bottom: 60px;
				  background: url('/Utopia/img/dartboard.jpeg') no-repeat center center fixed;
				  -webkit-background-size: cover;
				  -moz-background-size: cover;
				  -o-background-size: cover;
				  background-size: cover;
				  color:#fff;
				  background-color:#eee;
				  font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
				  overflow-y:scroll;
				}
				
				.form-group{
					padding: 5px;
				}

				.input-group-addon {
					min-width: 95px;
					text-align: left;
				}

				.navbar-nav > li{
					min-width: 90px;
					text-align: center;
				}

				#advanced{
					border-top: 1px solid #888;
					padding: 0px;
					margin: 0px;
				}

				.dropdown{
					padding: 0px;
					margin: 0;
					height: 25px;
					max-width: 200px;
					min-width: 100px;
					color: gray;
					border-radius: 4px;
				}
				
				.options{
					min-width: 100px;
				}

				.smaller{
					max-width: 100px;
					max-height: 22px;
					padding: 5px;
					margin: 0;
				}

				.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
				  background-color: #111;
				}

				.table-hover tbody tr td, .table-hover tbody tr th {
				  max-width: 280px;
				}

				.btn-link, .btn-link:hover, .btn-link:active, .btn-link:focus{
					color: white;
					border: 1px solid white;
					border-radius: 4px;
				}

			</style>
		</head>

		<body id="homearea">
			<div class="showOnLoad" >
			
			<?php
				require('template/navigation.php');
			?>

			<section class="container-fluid" style="margin-top:50px; background-color: rgba(55,55,55,0.5);
					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.19);">
				<article style="opacity: none;">
					<h3 style="color: #EEEEEE;">Simplify your target search:</h3>
					<form action="/Utopia/db2json.php" id="provinceFinder" method="post">
						<div class="form-group col-sm-3">
							<input class="form-control " type="number" name="selfNW" placeholder="Your networth here" />
						</div>
						<div class="form-group col-sm-3">
							<input class="form-control" type="number" name="selfAcre" placeholder="Your acreage here" />
						</div>
						<div class="form-group col-sm-3">
							<input class="form-control" type="number" name="kdNW" placeholder="KD networth here(optional)" />
						</div>
						<div class="form-group col-sm-2">
							<input type="submit" id="goButton" class="btn btn-danger btn-block" value="GO!" />
						</div>
						<div class="form-group col-sm-1">
							<button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#advanced" type="button">
								<i class="glyphicon glyphicon glyphicon-filter"></i>
							</button>
						</div>
						<input name="action" type="hidden" value="provinces" />
						<div class="col-sm-12 collapse" id="advanced" style="margin: 5px;">
							<div class="col-sm-12">
								<h4>Basic Controls:</h4>
								<div class="col-sm-3" >
									<label for="race">Race : <br/>
									<div class="btn-group">
									  <button type="button" class="btn btn-link btn-xs checkAllRace">Select All</button>
									  <button type="button" class="btn btn-link btn-xs uncheckAllRace">Deselect All</button>
									</div>
									</label><br/>
										<label class="options">
										<input class="race" type="checkbox" name="avian" value="avian" checked/> Avian</label>
										<label class="options">
										<input class="race" type="checkbox" name="dwarf" value="dwarf" checked/> Dwarf</label>
										<label class="options">
										<input class="race" type="checkbox" name="elf" value="elf" checked/> Elf</label>
										<label class="options">
										<input class="race" type="checkbox" name="faery" value="faery" checked/> Faery</label>
										<label class="options">
										<input class="race" type="checkbox" name="human" value="human" checked/> Human</label>
										<label class="options">
										<input class="race" type="checkbox" name="halfer" value="halfling" checked/> Halfling</label>
										<label class="options">
										<input class="race" type="checkbox" name="orc" value="orc" checked/> Orc</label>
										<label class="options">
										<input class="race" type="checkbox" name="undead" value="undead" checked/> Undead</label>
										<label class="options">
										<input class="race" type="checkbox" name="dryad" value="dryad" checked/> Dryad</label>
								</div>
								<div class="col-sm-3">
									<label for="stance">Stance: <br/>
									<div class="btn-group">
									  <button type="button" class="btn btn-link btn-xs checkAllStance">Select All</button>
									  <button type="button" class="btn btn-link btn-xs uncheckAllStance">Deselect All</button>
									</div>
									</label><br/>
										<label class="options">
										<input class="stance" type="checkbox" name="normal" value="normal" checked/> Normal</label>
										<label class="options">
										<input class="stance" type="checkbox" name="war" value="war" /> War</label>
										<label class="options">
										<input class="stance" type="checkbox" name="eowcf" value="EoWCF" value="eowcf" /> EoWCF</label>
										<label class="options">
										<input class="stance" type="checkbox" name="fort" value="fort" /> Fort</label>
										<label class="options">
										<input class="stance" type="checkbox" name="aggro" value="aggressive" checked/> Aggressive</label>
								</div>
								<div class="col-sm-3" >
									<label for="race">Probabilistic calc:</label><br/>
										<label class="options"><input type="checkbox" name="gbp" value="show" disabled /> GBP status</label>
										<label class="options"><input type="checkbox" name="prot" value="show" checked/> Hide Protected</label>
								</div>
								<div class="col-sm-3" >
									<label style="margin:0; padding:0;">Stance modifiers:</label>
									<div class="form-group" style="margin:0; padding:0 0 0 10px;">
										<label for="stancefrom" style="margin:0; padding:0;">From:</label>
										<select class="form-control dropdown" id="stancefrom">
											<option>--Any--</option>
											<option>Aggressive</option>
											<option>EoWCF</option>
											<option>Fortified</option>
											<option>Normal</option>
											<option>War</option>
										</select>
										<label for="stanceto" style="margin:0; padding:0;">To:</label>
										<select class="form-control dropdown" id="stanceto">
											<option>--Any--</option>
											<option>Aggressive</option>
											<option>EoWCF</option>
											<option>Fortified</option>
											<option>Normal</option>
											<option>War</option>
										</select>
									</div>
									<div class="input-group input-group-sm" style="padding-bottom: 20px;">
										<label class="options" style="margin:10px 0 0px 0;">Tick Range :</label>
										<div class="form-group" style="margin:0; padding:0 0 0 10px;">
											<input class="form-control smaller" type="number" name="hrAgo" placeholder="-hr" min="0" step="1" />
											<input class="form-control smaller" type="number" name="hrAhead" placeholder="+hr" min="0" step="1" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<h4>Other controls:</h4>
								<div class="col-sm-3">
									<label for="stance">Micromanaging:</label><br/>
										<div class="input-group input-group-sm">
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minNW" placeholder="Min NW" />
												<input class="form-control smaller" type="number" name="maxNW" placeholder="Max NW" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minAc" placeholder="Min Acre" />
												<input class="form-control smaller" type="number" name="maxAc" placeholder="Max Acre" />
											</div>
											<div class="form-group">
												<select class="form-control smaller" name="minHon" 
												style="padding: 0px;">
													<option value="-1">-Min honor-</option>
													<option value="0">Peasant</option>
													<option value="1">Knight/Lady</option>
													<option value="2">Lord/Noble Lady</option>
													<option value="3">Baron/Baroness</option>
													<option value="4">Viscount/Viscountess</option>
													<option value="5">Count/Countess</option>
													<option value="6">Marquis/Marchioness</option>
													<option value="7">Duke/Duchess</option>
													<option value="8">Prince/Princess</option>
												</select>
												<select class="form-control smaller" name="maxHon"
												style="padding: 0px;">
													<option value="9">-Max honor-</option>
													<option value="0">Peasant</option>
													<option value="1">Knight/Lady</option>
													<option value="2">Lord/Noble Lady</option>
													<option value="3">Baron/Baroness</option>
													<option value="4">Viscount/Viscountess</option>
													<option value="5">Count/Countess</option>
													<option value="6">Marquis/Marchioness</option>
													<option value="7">Duke/Duchess</option>
													<option value="8">Prince/Princess</option>
												</select>
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minkdnw" placeholder="Min KD NW" />
												<input class="form-control smaller" type="number" name="maxkdnw" placeholder="Max KD NW" />
											</div>
									</div>
								</div>
								<div class="col-sm-3">
									<label for="stance">More Micromanaging:</label><br/>
										<div class="input-group input-group-sm">
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minIsl" placeholder="From island" />
												<input class="form-control smaller" type="number" name="maxIsl" placeholder="To island" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minPVnum" placeholder="Min #prov" />
												<input class="form-control smaller" type="number" name="maxPVnum" placeholder="Max #prov" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="text" name="loc" placeholder="Location" />
												<input class="form-control smaller" type="text" name="pvname" placeholder="Prov name" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minWW" placeholder="Min WarWin" />
												<input class="form-control smaller" type="number" name="maxWW" placeholder="Max WarWin" />
											</div>
									</div>
								</div>
								<div class="col-sm-3">
									<label for="ceasefire">Ceasefires:</label><br/>
										<textarea name="ceasefire" placeholder="Paste CF page or comma separated location list." 
										rows="2" cols="25" style="color: black; resize: none;"></textarea>
								</div>
								<div class="col-sm-3">
									
								</div>
							</div>
						</div>
					</form>
				</article>
			</section>

			<div class="container table-responsive shadowBG" id="searchResult">
				<?php
					require('template/alerts.php');
				?>
			</div>

			<?php
				require('template/footer.php');
			?>
			</div>

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="/Utopia/js/__jquery.tablesorter/jquery.tablesorter.js"></script>
			<script src='/Utopia/js/tf.js'></script>

			<script type="text/javascript">
			
			</script>

		</body>
	</html>