	<!DOCTYPE html>
	<html>
		<head>
			<?php 
				require('template/head.php');
			?>
			<style type="text/css">

				body {
				  /* Margin bottom by footer height */
				  margin-bottom: 60px;
				  background: url('/Utopia/img/citylights.jpeg') no-repeat center center fixed;
				  -webkit-background-size: cover;
				  -moz-background-size: cover;
				  -o-background-size: cover;
				  background-size: cover;
				  color:#fff;
				  background-color:#eee;
				  font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
				  overflow-y:scroll;
				}

				.form-group{
					padding: 5px;
				}

				.input-group-addon {
					min-width: 95px;
					text-align: left;
				}

				.navbar-nav > li{
					min-width: 90px;
					text-align: center;
				}

				#advanced{
					border-top: 1px solid #888;
					padding: 0px;
					margin: 0px;
				}

				.options{
					min-width: 100px;
				}

				.smaller{
					max-width: 100px;
					max-height: 22px;
					padding: 5px;
					margin: 0;
				}

				.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
				  background-color: #111;
				}

				.table-hover tbody tr td, .table-hover tbody tr th {
				  max-width: 280px;
				}

				.btn-link, .btn-link:hover, .btn-link:active, .btn-link:focus{
					color: white;
					border: 1px solid white;
					border-radius: 4px;
				}

			</style>
		</head>

		<body id="homearea">
			<div class="showOnLoad" >
			
			<?php
				require('template/navigation.php');
			?>

			<section class="container" style="margin-top:70px; background-color: rgba(55,55,55,0.5);
					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.19);">
				<article style="opacity: none;">
					<h3 style="color: #EEEEEE; margin-top: 15px;">Search past issues or feedbacks:</h3>
					<form action="" id="ticketFinder" method="get">	
						<div class="form-group col-sm-8">
							<input class="form-control" type="text" name="search" placeholder="Search text here..." />
						</div>
						<div class="form-group col-sm-2">
							<select class="form-control" id="searchType">
							  <option value="tktid">Ticket ID</option>
							  <option value="tktAuth">Author</option>
							  <option value="tktTag">Title</option>
							  <option value="tktStat">Status</option>
							</select>
						</div>
						<div class="form-group col-sm-2">
							<button type="submit" id="goButton" class="btn btn-success btn-block" />
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
						<input name="action" type="hidden" value="getTicket" />
					</form>
				</article>
			</section>

			<div class="container table-responsive shadowBG" id="searchResult" hidden="hidden" 
			style="margin-bottom: 10px;">
			</div>

			<div class="container shadowBG" id="postResult" 
			style="margin-top:20px; background-color: rgba(55,55,55,0.5); box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.19);">
				<h3 style="color: #EEEEEE; margin-top: 5px;">Report a new issue or send a feedback:</h3>
				<form action="" id="submitFeedback" method="post">	
					<div class="form-group col-sm-12">
						<input class="form-control input-lg" type="text" id="FDBtitle" name="FDBtitle" placeholder="Title" maxlength="50" required/>
					</div>
					<div class="form-group">
					  <textarea class="form-control" rows="5" id="comment" 
					  style="resize: vertical;" placeholder="Details..." maxlength='1024' required></textarea>
					</div>
					<div class="form-group col-sm-2">
						<select class="form-control" id="FDBtype">
						  <option value="feedback">Feedback</option>
						  <option value="issue" selected>Issue</option>
						</select>
					</div>
					<div class="form-group col-sm-5">
						<input class="form-control" id="FDBemail" type="email" 
						name="FDBemail" placeholder="E-mail ID" maxlength="50"/>
					</div>
					<div class="form-group col-sm-5">
						<input class="form-control" id="FDBauth" type="text" 
						name="FDBauth" placeholder="Author" disabled/>
					</div>
					<input id="action" type="text" name="action" value="feedback_issue" hidden="hidden" disabled/>
					<div class="form-group col-sm-12">
						<button type="submit" id="FDBsubmit" class="btn btn-primary btn-block btn-lg" />
							SUBMIT
						</button>
					</div>
					<input name="action" type="hidden" value="feedbackSearch" />
				</form>
			</div>

			<div id="tktModal" class="modal fade" role="dialog">
				<div class="modal-dialog" style="color: gray; width: 80%;">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="modal-title">Ticket Header</h3>
							<span style="font-size: 0.8em;">AUTHOR : 
								<span class="modal-auth" style="font-size: 1em;">Auth_name</span>
							</span><br/>
							<span style="font-size: 0.8em;">Originated : 
								<i><span class="modal-origin" style="font-size: 1em;">Origin date</span></i>
							</span>
						</div>
						<div class="modal-body">
							<p>Ticket description.</p>
						</div>
						<div class="modal-footer">
							<div class="modal-status" style="">STATUS : 
								<button type="button" class="btn btn-primary btn-xs">Status</button>
							</div>
						</div>
					</div>

				</div>
			</div>

			<?php
				require('template/footer.php');
			?>
			</div>

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="/Utopia/js/__jquery.tablesorter/jquery.tablesorter.js"></script>					
			<script src='/Utopia/js/fdb.js'></script>

			<script type="text/javascript">
				$('#FDBemail').keyup(function(){
					var email = $('#FDBemail').val();
					var arr = email.split('@');
					// console.log('Hello: '+arr);
					$('#FDBauth').empty().val(arr[0]);
				});
			</script>

		</body>
	</html>