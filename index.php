<!DOCTYPE html>
<html>
	<head>
		<?php 
			require('template/head.php');
		?>
		<style type="text/css">

			body {
			  /* Margin bottom by footer height */
			  margin-bottom: 60px;
			  background: url('/Utopia/img/green.jpeg') no-repeat center center fixed;
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
			  color:#fff;
			  background-color:#eee;
			  font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
			  overflow-y:scroll;
			}

			.form-group{
				padding: 5px;
			}

			.input-group-addon {
				min-width: 95px;
				text-align: left;
			}

			.navbar-nav > li{
				min-width: 90px;
				text-align: center;
			}

			#advanced{
				border-top: 1px solid #888;
				padding: 0px;
				margin: 0px;
			}

			.options{
				min-width: 100px;
			}

			.smaller{
				max-width: 100px;
				max-height: 22px;
				padding: 5px;
				margin: 0;
			}

			.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
			  background-color: #111;
			}

			.table-hover tbody tr td, .table-hover tbody tr th {
			  max-width: 280px;
			}

			.btn-link, .btn-link:hover, .btn-link:active, .btn-link:focus{
				color: white;
				border: 1px solid white;
				border-radius: 4px;
			}

		</style>
	</head>

	<body id="homearea">
		<div class="showOnLoad" >
		
		<?php
			require('template/navigation.php');
		?>

		<section class="container" style="margin-top: 70px; margin-bottom: 10px;">
			<article class="jumbotron text-center" 
					 style="background-color: rgba(55,55,55,0);
					 font-family: 'Courier';
					 margin-bottom: 10px;">
				<h1 style="margin: 10px auto 10px auto;">
					<small style="padding: 10px 0 10px 0; color: #F75;">TreasonGuy's:</small>
					<kbd style="font-family: 'Courier'; padding: 10px; min-width: 350px;">
					UTOINTEL</kbd>
				</h1>
			</article>
		</section>

		<div class="container shadowBG" id="searchResult" 
		style="margin-top:20px;
			margin-bottom: 40px;
			background-color: rgba(55,55,55,0.5); 
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.19);">
			<div class="col-sm-4 text-center">
				<h4 style="margin-top:15px;">Total Provinces : 
					<span class="count" id="pv" style="color: #0F0;"></span>
				</h4>
			</div>
			<div class="col-sm-4 text-center">
				<h4 style="margin-top:15px;">Total Kingdoms : 
					<span class="count" id="kd" style="color: #0F0;"></span>
				</h4>
			</div>
			<div class="col-sm-4 text-center">
				<h4 style="margin-top:15px;">Last Updated : 
					<span class="count" id="upd" style="color: #0F0;"></span>
				</h4>
			</div>
			<div class="col-sm-12 progress" 
				style="padding: 0; margin-top: 30px; height: 30px; background-color: gray;">
			    <div class="progress-bar progress-bar-success progress-bar-striped" 
			    	id="fortBar" role="progressbar" style="color: #333; padding-top: 4px;">
			    </div>
			    <div class="progress-bar progress-bar-info progress-bar-striped" 
			    	id="normBar" role="progressbar" style="color: #333; padding-top: 4px;">
			    </div>
			    <div class="progress-bar progress-bar-warning progress-bar-striped" 
			    	id="aggrBar" role="progressbar" style="color: #333; padding-top: 4px;">
			    </div>
			    <div class="progress-bar progress-bar-danger progress-bar-striped" 
			    	id="warBar" role="progressbar" style="color: #333; padding-top: 4px;">
			    </div>
			</div>
			<div class="col-sm-3 ">
			 	<button class="btn btn-link btn-block" id="fortLegend">FORT</button>
			</div>
			<div class="col-sm-3 ">
			  	<button class="btn btn-link btn-block" id="normalLegend">NORMAL</button>
			</div>
			<div class="col-sm-3 ">
			  	<button class="btn btn-link btn-block" id="aggroLegend">AGGRESSIVE</button>
			</div>
			<div class="col-sm-3 ">
			  	<button class="btn btn-link btn-block" id="warLegend">WAR</button>
			</div>
		</div>

		<div class="container table-responsive shadowBG" id="searchResult">
			<?php
				require('template/alerts.php');
			?>
		</div>

		<?php
			require('template/footer.php');
		?>
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/Utopia/js/__jquery.tablesorter/jquery.tablesorter.js"></script>					
		<script src='/Utopia/js/kf.js'></script>

		<script type="text/javascript">
			var url = "/Utopia/raceData.php";
			var posting = $.post( url, {action	: 'homepage'} );

			posting.done(function( data ) {
				error = 0;

				try{
					response = data;
					response = jQuery.parseJSON(data);
				}catch(e){
					$("#shadowBG").empty().append('<h4 style="color:pink;"><b>Error!</b> <br/>Something went terribly wrong.<br/>If problem persists, please contact admin. It would really help if you can paste the console log along. </h4>');
					console.log(e.message+data.substring(0,data.indexOf('{')));
					error = 1;
				}

				if (jQuery.isEmptyObject(response) || error == 1) {
					table = "No data found!";
				}
				else{
					$('#kd').each(function () {
					    $(this).prop('Counter',0).animate({
					        Counter: response.posts[0].kingdoms
					    }, {
					        duration: 1000,
					        easing: 'swing',
					        step: function (now) {
					            $(this).text(Math.ceil(now));
					        }
					    });
					});
					$('#pv').each(function () {
					    $(this).prop('Counter',0).animate({
					        Counter: response.posts[0].provinces
					    }, {
					        duration: 1000,
					        easing: 'swing',
					        step: function (now) {
					            $(this).text(Math.ceil(now));
					        }
					    });
					});
					var ts = Date.parse(response.posts[0].updated);
					var date = new Date($.now());
					var diff = (($.now()-ts)/1000/60)+date.getTimezoneOffset();
					var lapse = '';
					if (diff > 60) {
						lapse = '<span style="color: #F44;">'+parseInt(diff/60)+' hr ago</span>';
					}
					else{
						lapse = '<span style="color: #0F0;">'+parseInt(diff)+' min ago</span>';
					}
					$('#upd').html(lapse);

					var frt = Math.round(response.posts[0].fort/response.posts[0].kingdoms*100);
					var nrml = Math.round(response.posts[0].normal/response.posts[0].kingdoms*100);
					var aggr = Math.round(response.posts[0].aggro/response.posts[0].kingdoms*100);
					var warring = Math.round(100 - frt - nrml - aggr);

					$("#fortBar").animate({
					    width: frt + "%"
					}, 200);
					$("#normBar").animate({
					    width: nrml + "%"
					}, 200);
					$("#aggrBar").animate({
					    width: aggr + "%"
					}, 200);
					$("#warBar").animate({
					    width: warring + "%"
					}, 200);

					$("#fortBar").html('<b>' + frt +'%</b>');
					$("#normBar").html('<b>' + nrml +'%</b>');
					$("#aggrBar").html('<b>' + aggr +'%</b>');
					$("#warBar").html('<b>' + warring +'%</b>');

					$('#fortLegend').html('FORT : '+response.posts[0].fort);
					$('#normalLegend').html('NORMAL : '+response.posts[0].normal);
					$('#aggroLegend').html('AGGRESSIVE : '+response.posts[0].aggro);
					$('#warLegend').html('WAR : '+response.posts[0].war);
				}
			});

			$('#fortLegend').on('touchstart mouseover',function(){
				$('#fortBar').css('background-color', 'green');
			});
			$('#fortLegend').on('touchend mouseout',function(){
				$('#fortBar').css('background-color', '#5cb85c');
			});
			$('#normalLegend').on('touchstart mouseover',function(){
				$('#normBar').css('background-color', '#0055ff');
			});
			$('#normalLegend').on('touchend mouseout',function(){
				$('#normBar').css('background-color', '#5bc0de');
			});
			$('#aggroLegend').on('touchstart mouseover',function(){
				$('#aggrBar').css('background-color', '#bbbbbb');
			});
			$('#aggroLegend').on('touchend mouseout',function(){
				$('#aggrBar').css('background-color', '#f0ad4e');
			});
			$('#warLegend').on('touchstart mouseover',function(){
				$('#warBar').css('background-color', '#ff2222');
			});
			$('#warLegend').on('touchend mouseout',function(){
				$('#warBar').css('background-color', '#d9534f');
			});
		</script>

	</body>
</html>