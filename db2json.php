<?php
	require('db_conn.php');
	$dbgstr = 'DEBUG: ';

	if(isset($_POST['action'])){
		if($_POST['action'] == 'provinces'){
			$Q_nwRangePV = '';
			$Q_nwRangeKD = '';
			$Q_landRangeKD = '';
			$Q_honorRangePV = '';
			$Q_countRangeKD = '';
			$Q_validStances = '';
			$Q_warwinRangeKD = '';
			$Q_locPV = '';
			$Q_namePV = '';
			$Q_islPV = '';
			$Q_validRaces = '';
			$Q_ceasefires = '';
			$Q_stanceChange = '';

			// === Province Networth ===
			// ****** QUERY CALC  ******
			// =========================
			if ($_POST['minNW'] != "" and $_POST['maxNW'] != "") {
				$Q_nwRangePV = ' provinces.nw BETWEEN '.min($_POST['minNW'],$_POST['maxNW']).' AND '.max($_POST['minNW'],$_POST['maxNW']);
			}
			else if ($_POST['selfNW'] != "" and $_POST['minNW'] != ""){
				$Q_nwRangePV = ' provinces.nw BETWEEN '.min($_POST['minNW'],($_POST['selfNW']*1.20)).' AND '.max(($_POST['selfNW']*1.20),$_POST['minNW']);
			}
			else if ($_POST['selfNW'] != "" and $_POST['maxNW'] != ""){
				$Q_nwRangePV = ' provinces.nw BETWEEN '.min($_POST['maxNW'],($_POST['selfNW']*0.85)).' AND '.max($_POST['maxNW'],($_POST['selfNW']*0.85));
			}
			else if ($_POST['selfNW'] != ""){
				$Q_nwRangePV = ' provinces.nw BETWEEN '.($_POST['selfNW']*0.85).' AND '.($_POST['selfNW']*1.20);
			}
			else{
				$Q_nwRangePV = '';
			}

			// === Kingdom Networth ===
			// ****** QUERY CALC ******
			// ========================
			if ($_POST['minkdnw'] != "" and $_POST['maxkdnw'] != "") {
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.min($_POST['minkdnw'],$_POST['maxkdnw']).' AND '.max($_POST['minkdnw'],$_POST['maxkdnw']);
			}
			else if ($_POST['minkdnw'] == "" and $_POST['maxkdnw'] != "" and $_POST['kdNW'] == ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth < '.$_POST['maxkdnw'];
			}
			else if ($_POST['minkdnw'] != "" and $_POST['maxkdnw'] == "" and $_POST['kdNW'] == ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth > '.$_POST['minkdnw'];
			}
			else if ($_POST['kdNW'] != "" and $_POST['minkdnw'] != ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.min($_POST['minkdnw'],($_POST['kdNW']*1.30)).' AND '.max($_POST['minkdnw'],($_POST['kdNW']*1.30));
			}
			else if ($_POST['kdNW'] != "" and $_POST['maxkdnw'] != ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.min($_POST['maxkdnw'],($_POST['kdNW']*0.70)).' AND '.max($_POST['maxkdnw'],($_POST['kdNW']*0.70));
			}
			else if ($_POST['kdNW'] != ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.($_POST['kdNW']*0.70).' AND '.($_POST['kdNW']*1.30);
			}
			else{
				$Q_nwRangeKD = '';
			}

			// ======== Kingdom Stance Change =======
			// ******        QUERY CALC        ******
			// ======================================
			$timezone = new DateTimeZone(date_default_timezone_get());  
			$date = new DateTime(gmdate('Y-m-d H:i:s'));
			$Q_pre  = '';
			$Q_post = '';
			$dbgstr .= $_POST['hrAhead'].$_POST['stancefrom'].$_POST['stanceto'].$_POST['hrAgo'];
			if ($_POST['hrAhead'] > 0){
				$posttime = clone $date;
				$posttime->add(new DateInterval('PT'.$_POST['hrAhead'].'H'));
				$posttime->sub(new DateInterval('PT'.$posttime->format("i").'M'));
				$posttime->sub(new DateInterval('PT'.$posttime->format("s").'S'));
				$Q_post = " kingdoms.stanceEnd <= '".$posttime->format('Y-m-d h:i:s')."' ";

				if ($_POST['stancefrom'] != "--Any--") {
					$Q_post = " UPPER(kingdoms.stance) = '".strtoupper($_POST['stancefrom'])."' AND ".$Q_post;
				}
			}
			if ($_POST['hrAgo'] > 0) {
				$pretime = clone $date;
				$pretime->sub(new DateInterval('PT'.($_POST['hrAgo']).'H'));
				$pretime->sub(new DateInterval('PT'.max($pretime->format("i"),($pretime->format("i")-3)).'M'));
				$pretime->sub(new DateInterval('PT'.$pretime->format("s").'S'));
				$dbgstr .= " CURR: ".$date->format('Y-m-d h:i:s')." PRE: ".$pretime->format('Y-m-d h:i:s')." INTERVAL: -".$_POST['hrAgo'];
				$Q_pre = " UPPER(kingdoms.lastStance) IS NOT NULL AND kingdoms.lastStanceChange >= '".$pretime->format('Y-m-d h:i:s')."' ";

				if ($_POST['stancefrom'] != "--Any--" and $_POST['stanceto'] != "--Any--") {
					$Q_pre = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."' AND UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."' AND ".$Q_pre;
				}
				elseif ($_POST['stancefrom'] != "--Any--") {
					$Q_pre = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."' AND ".$Q_pre;
				}
				elseif ($_POST['stanceto'] != "--Any--") {
					$Q_pre = " UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."' AND ".$Q_pre;
				}
			}
			
			if ($Q_pre != '' and $Q_post != '') {
				$Q_stanceChange = " ((".$Q_pre.") OR (".$Q_post.")) ";
			}
			elseif ($Q_pre != '') {
				$Q_stanceChange = " (".$Q_pre.") ";
			}
			elseif ($Q_post != '') {
				$Q_stanceChange = " (".$Q_post.") ";
			}
			elseif ($Q_pre == '' and $Q_post == '') {
				if ($_POST['stancefrom'] != "--Any--" and $_POST['stanceto'] != "--Any--") {
					$Q_stanceChange = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."' AND UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."'";
				}
				elseif ($_POST['stancefrom'] != "--Any--") {
					$Q_stanceChange = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."'";
				}
				elseif ($_POST['stanceto'] != "--Any--") {
					$Q_stanceChange = " UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."'";
				}
			}

			// ===== Province Land =====
			// ******  QUERY CALC ******
			// =========================
			if ($_POST['minAc'] != "" and $_POST['maxAc'] != "") {
				$Q_landRangeKD = ' provinces.land BETWEEN '.min($_POST['minAc'],$_POST['maxAc']).' AND '.max($_POST['minAc'],$_POST['maxAc']);
			}
			else if ($_POST['minAc'] == "" and $_POST['maxAc'] != "" and $_POST['selfAcre'] == ""){
				$Q_nwRangeKD = ' provinces.land < '.$_POST['maxAc'];
			}
			else if ($_POST['minAc'] != "" and $_POST['maxAc'] == "" and $_POST['selfAcre'] == ""){
				$Q_nwRangeKD = ' provinces.land > '.$_POST['minAc'];
			}
			else if ($_POST['maxAc'] != "" and $_POST['selfAcre'] != "") {
				$Q_landRangeKD = ' provinces.land BETWEEN '.min($_POST['maxAc'],($_POST['selfAcre']*0.70)).' AND '.max($_POST['maxAc'],($_POST['selfAcre']*0.70));
			}
			else if ($_POST['selfAcre'] != "" and $_POST['minAc'] != ""){
				$Q_nwRangeKD = ' provinces.land BETWEEN '.min($_POST['minAc'],($_POST['selfAcre']*1.30)).' AND '.max($_POST['minAc'],($_POST['selfAcre']*1.30));
			}
			else if ($_POST['selfAcre'] != "") {
				$Q_landRangeKD = ' provinces.land BETWEEN '.($_POST['selfAcre']*0.85).' AND '.($_POST['selfAcre']*1.20);
			}
			else{
				$Q_landRangeKD = '';
			}

			// ===== Province Honor =====
			// ******  QUERY CALC  ******
			// ==========================
			$honorLvl = array();
			if(min($_POST['minHon'],$_POST['maxHon']) != -1 or max($_POST['minHon'],$_POST['maxHon']) != 9){
				switch ((string)min($_POST['minHon'],$_POST['maxHon'])) {
					case '-1': 
					case '0' : array_push($honorLvl,"Peasant");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 0) {
							break;
						}
					case '1' : array_push($honorLvl,"Knight/Lady");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 1) {
							break;
						}
					case '2' : array_push($honorLvl,"Lord/Noble Lady");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 2) {
							break;
						}
					case '3' : array_push($honorLvl,"Baron/Baroness");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 3) {
							break;
						}
					case '4' : array_push($honorLvl,"Viscount/Viscountess");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 4) {
							break;
						}
					case '5' : array_push($honorLvl,"Count/Countess");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 5) {
							break;
						}
					case '6' : array_push($honorLvl,"Marquis/Marchioness");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 6) {
							break;
						}
					case '7' : array_push($honorLvl,"Duke/Duchess");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 7) {
							break;
						}
					case '8' : array_push($honorLvl,"Prince/Princess");
						if ((int)max($_POST['minHon'],$_POST['maxHon']) <= 8) {
							break;
						}
					default : break;
				}
				if(!empty($honorLvl)){
					$Q_honorRangePV = " provinces.honor IN ('".join(',',$honorLvl).") ";
				}
			}

			// ===== Kingdom War Wins =====
			// ******   QUERY CALC   ******
			// ============================
			if ($_POST['minWW'] != "" and $_POST['maxWW'] != "") {
				$Q_warwinRangeKD = ' kingdoms.warWin BETWEEN '.min($_POST['minWW'],$_POST['maxWW']).' AND '.max($_POST['minWW'],$_POST['maxWW']);
			}
			else if ($_POST['minWW'] != "") {
				$Q_warwinRangeKD = ' kingdoms.warWin >= '.$_POST['minWW'];
			}
			else if ($_POST['maxWW'] != "") {
				$Q_warwinRangeKD = ' kingdoms.warWin <= '.$_POST['maxWW'];
			}
			else{
				$Q_warwinRangeKD = '';
			}

			// ===== Kingdom PV Count =====
			// ******   QUERY CALC   ******
			// ============================
			if ($_POST['minPVnum'] != "" and $_POST['maxPVnum'] != "") {
				$Q_countRangeKD = ' kingdoms.count BETWEEN '.min($_POST['minPVnum'],$_POST['maxPVnum']).' AND '.max($_POST['minPVnum'],$_POST['maxPVnum']);
			}
			else if ($_POST['minPVnum'] != "") {
				$Q_countRangeKD = ' kingdoms.count > '.$_POST['minPVnum'];
			}
			else if ($_POST['maxPVnum'] != "") {
				$Q_countRangeKD = ' kingdoms.count < '.$_POST['maxPVnum'];
			}
			else{
				$Q_countRangeKD = '';
			}

			// ===== Province Location =====
			// ******    QUERY CALC   ******
			// =============================
			if ($_POST['loc'] != "") {
				$Q_locPV = " provinces.loc = '".$_POST['loc']."'";
			}

			// ===== Protection status =====
			// ******    QUERY CALC   ******
			// =============================
			if ($_POST['prot'] == "true") {
				$Q_protPV = " provinces.protected = 0 ";
			}

			// ==== Province Name =====
			// ****** QUERY CALC ******
			// ========================
			if ($_POST['pvname'] != "") {
				$Q_namePV = " UPPER(provinces.name) LIKE '%".strtoupper($_POST['pvname'])."%'";
			}

			// ===== Province Island =====
			// ******   QUERY CALC  ******
			// ===========================
			$start = 1;
			$end = 8;
			$allIsl = 0;
			$queryArr = array();
			if ($_POST['minIsl'] != "" and $_POST['maxIsl'] != "") {
				$start = min((int)$_POST['minIsl'],(int)$_POST['maxIsl']);
				$end = max((int)$_POST['minIsl'],(int)$_POST['maxIsl']);
			}
			else if ($_POST['minIsl'] != "") {
				$start = (int)$_POST['minIsl'];
			}
			else if ($_POST['maxIsl'] != "") {
				$end = (int)$_POST['maxIsl'];
			}
			else{
				$allIsl = 1;
			}
			if ($allIsl != 1) {
				foreach (range($start, $end) as $num) {
					array_push($queryArr, " provinces.loc LIKE '%:".$num."'");
				}
				$Q_islPV = " (".join(' OR ',$queryArr).") ";
			}

			// ===== Province Race ======
			// ******  QUERY CALC  ******
			// ==========================
			$races = array('avian', 'dwarf', 'elf', 'faery', 'halfling', 'human', 'orc', 'undead', 'dryad');
			$checkCTR = 0;
			$validRaces = array();
			foreach ($races as $r) {
				if(isset($_POST[$r])){
					if($_POST[$r] == 'true'){
						array_push($validRaces, strtoupper($r));
						$checkCTR += 1;
					}
				}
			}
			if ($checkCTR == sizeof($races)) {
				$Q_validRaces = '';
			}
			else if(!empty($validRaces)){
				$Q_validRaces = " UPPER(provinces.race) IN ('".join("','",$validRaces)."') ";
			}

			// ===== Province Stance =====
			// ******   QUERY CALC  ******
			// ===========================
			$stances = array('fortified', 'normal', 'eowcf', 'aggressive', 'war');
			$checkCTR = 0;
			$validStances = array();
			foreach ($stances as $r) {
				if(isset($_POST[$r])){
					if($_POST[$r] == "true"){
						array_push($validStances, strtoupper($r));
						$checkCTR += 1;
					}
				}
			}
			if ($checkCTR == sizeof($stances)) {
				$Q_validStances = "";
			}
			else if(!empty($validStances)){
				$Q_validStances = " UPPER(kingdoms.stance) IN ('".join("','",$validStances)."') ";
			}

			// ===== KD CFs =====
			// *** QUERY CALC ***
			// ==================
			$CFstring = "We currently have ceasefire agreements in place with the following kingdoms:";
			$CFendString = "Outstanding Ceasefire Proposals";
			$pos = strpos($_POST['ceasefire'], $CFstring);
			$posEnd = strpos($_POST['ceasefire'], $CFendString);
			$posLoc = 0;
			$CFkd = array();
			if ($pos !== false) {
				$posLoc = strpos($_POST['ceasefire'], ' (', $pos);
				while ($posLoc !== false and $posLoc < $posEnd) {
					if ($posLoc !== false) {
						$locEnd = strpos($_POST['ceasefire'], ')', $posLoc);
						array_push($CFkd, substr($_POST['ceasefire'], $posLoc + 2, $locEnd - $posLoc - 2));
					}
					$pos = $locEnd+1;
					$posLoc = strpos($_POST['ceasefire'], ' (', $pos);
				}
			}
			else if($_POST['ceasefire']){
				$CFstr = $_POST['ceasefire'];
				for($i=0;$i<100;$i++){
				if(preg_match("/(\d+:\d+)/", $CFstr, $match)){
					array_push($CFkd, $match[1]);
					$CFstr = preg_replace("/$match[1]/",	"", $CFstr);
					$dbgstr .= " IN: ".$match[1]." NOW: ".$CFstr;
				}}
			}

			if(!empty($CFkd)){
				$Q_ceasefires = " provinces.loc NOT IN ('".join("','",$CFkd)."') ";
			}

			// ===== Building Final Query =====
			// ***    FILTER QUERY CALC     ***
			// ================================
			$Q_filter = " WHERE ";
			$Q_filter .= ($Q_nwRangePV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_nwRangePV
							:
								$Q_nwRangePV
						:
							'';
			$Q_filter .= ($Q_nwRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_nwRangeKD
							:
								$Q_nwRangeKD
						:
							'';
			$Q_filter .= ($Q_landRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_landRangeKD
							:
								$Q_landRangeKD
						:
							'';
			$Q_filter .= ($Q_stanceChange != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_stanceChange
							:
								$Q_stanceChange
						:
							'';
			$Q_filter .= ($Q_honorRangePV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_honorRangePV
							:
								$Q_honorRangePV
						:
							'';
			$Q_filter .= ($Q_warwinRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_warwinRangeKD
							:
								$Q_warwinRangeKD
						:
							'';
			$Q_filter .= ($Q_countRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_countRangeKD
							:
								$Q_countRangeKD
						:
							'';
			$Q_filter .= ($Q_locPV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_locPV
							:
								$Q_locPV
						:
							'';
			$Q_filter .= ($Q_protPV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_protPV
							:
								$Q_protPV
						:
							'';
			$Q_filter .= ($Q_namePV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_namePV
							:
								$Q_namePV
						:
							'';
			$Q_filter .= ($Q_islPV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_islPV
							:
								$Q_islPV
						:
							'';
			$Q_filter .= ($Q_validRaces != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_validRaces
							:
								$Q_validRaces
						:
							'';
			$Q_filter .= ($Q_validStances != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_validStances
							:
								$Q_validStances
						:
							'';
			$Q_filter .= ($Q_ceasefires != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_ceasefires
							:
								$Q_ceasefires
						:
							'';

			$filterQuery = "SELECT 	provinces.name, 
									provinces.land, 
									provinces.loc, 
									provinces.protected, 
									provinces.race, 
									provinces.online, 
									provinces.honor, 
									kingdoms.kdNetworth,
									kingdoms.stance, 
									kingdoms.count AS kdcount, 
									kingdoms.land AS kdland, 
									kingdoms.name AS kdname, 
									kingdoms.honor AS kdhonor, 
									provinces.nw, 
									ROUND((provinces.nw / provinces.land), 2) AS nwpa
							FROM provinces 
							INNER JOIN kingdoms 
								ON kingdoms.loc = provinces.loc ";
			$filterQuery .= ($Q_filter != " WHERE ")?
								$Q_filter
							:
								'';

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'loc'		=>	$row['loc'],
								'land'		=>	number_format($row['land']),
								'name'		=>	$row['name'],
								'protected'	=>	$row['protected'],
								'race'		=>	$row['race'],
								'stance'	=>	$row['stance'],
								'online'	=>	$row['online'],
								'nwpa'		=>	$row['nwpa'],
								'honor'		=>	$row['honor'],
								'nw'		=>	number_format($row['nw']),
								'kdNetworth'=>	number_format($row['kdNetworth']));
			}
		}
		else if($_POST['action'] == 'kingdoms'){
			$Q_nwRangeKD = '';
			$Q_landRangeKD = '';
			$Q_honorRangeKD = '';
			$Q_countRangeKD = '';
			$Q_validStances = '';
			$Q_warwinRangeKD = '';
			$Q_warRangeKD = '';
			$Q_locPV = '';
			$Q_namePV = '';
			$Q_islPV = '';
			$Q_ceasefires = '';
			$Q_raceCount = '';
			$Q_stanceChange = '';

			// ===== KD CFs =====
			// *** QUERY CALC ***
			// ==================
			$CFstring = "We currently have ceasefire agreements in place with the following kingdoms:";
			$CFendString = "Outstanding Ceasefire Proposals";
			$pos = strpos($_POST['ceasefire'], $CFstring);
			$posEnd = strpos($_POST['ceasefire'], $CFendString);
			$posLoc = 0;
			$CFkd = array();
			if ($pos !== false) {
				$posLoc = strpos($_POST['ceasefire'], ' (', $pos);
				while ($posLoc !== false and $posLoc < $posEnd) {
					if ($posLoc !== false) {
						$locEnd = strpos($_POST['ceasefire'], ')', $posLoc);
						array_push($CFkd, substr($_POST['ceasefire'], $posLoc + 2, $locEnd - $posLoc - 2));
					}
					$pos = $locEnd+1;
					$posLoc = strpos($_POST['ceasefire'], ' (', $pos);
				}
			}
			if(!empty($CFkd)){
				$Q_ceasefires = " kingdoms.loc NOT IN ('".join("','",$CFkd)."') ";
			}

			// ===== Kingdom Stance =====
			// ******  QUERY CALC  ******
			// ==========================
			$stances = array('fortified', 'normal', 'eowcf', 'aggressive', 'war');
			$checkCTR = 0;
			$validStances = array();
			foreach ($stances as $r) {
				if(isset($_POST[$r])){
					if($_POST[$r] == "true"){
						array_push($validStances, strtoupper($r));
						$checkCTR += 1;
					}
				}
			}
			if ($checkCTR == sizeof($stances)) {
				$Q_validStances = "";
			}
			else if(!empty($validStances)){
				$Q_validStances = " UPPER(kingdoms.stance) IN ('".join("','",$validStances)."') ";
			}

			// ===== Kingdom War Wins =====
			// ******   QUERY CALC   ******
			// ============================
			if ($_POST['minWW'] != "" and $_POST['maxWW'] != "") {
				$Q_warwinRangeKD = ' kingdoms.warWin BETWEEN '.min($_POST['minWW'],$_POST['maxWW']).' AND '.max($_POST['minWW'],$_POST['maxWW']);
			}
			else if ($_POST['minWW'] != "") {
				$Q_warwinRangeKD = ' kingdoms.warWin >= '.$_POST['minWW'];
			}
			else if ($_POST['maxWW'] != "") {
				$Q_warwinRangeKD = ' kingdoms.warWin <= '.$_POST['maxWW'];
			}
			else{
				$Q_warwinRangeKD = '';
			}

			// ======== Kingdom Stance Change =======
			// ******        QUERY CALC        ******
			// ======================================
			$timezone = new DateTimeZone(date_default_timezone_get());  
			$date = new DateTime(gmdate('Y-m-d H:i:s'));
			$Q_pre  = '';
			$Q_post = '';

			if ($_POST['hrAhead'] > 0){
				$posttime = clone $date;
				$posttime->add(new DateInterval('PT'.$_POST['hrAhead'].'H'));
				$posttime->sub(new DateInterval('PT'.$posttime->format("i").'M'));
				$posttime->sub(new DateInterval('PT'.$posttime->format("s").'S'));
				$Q_post = " kingdoms.stanceEnd <= '".$posttime->format('Y-m-d h:i:s')."' ";

				if ($_POST['stancefrom'] != "--Any--") {
					$Q_post = " UPPER(kingdoms.stance) = '".strtoupper($_POST['stancefrom'])."' AND ".$Q_post;
				}
			}
			if ($_POST['hrAgo'] > 0) {
				$pretime = clone $date;
				$pretime->sub(new DateInterval('PT'.($_POST['hrAgo']).'H'));
				$pretime->sub(new DateInterval('PT'.($pretime->format("i")-10).'M'));
				$pretime->sub(new DateInterval('PT'.$pretime->format("s").'S'));
				$dbgstr .= " CURR: ".$date->format('Y-m-d h:i:s')." PRE: ".$pretime->format('Y-m-d h:i:s')." INTERVAL: -".$_POST['hrAgo'];
				$Q_pre = " UPPER(kingdoms.lastStance) IS NOT NULL AND kingdoms.lastStanceChange >= '".$pretime->format('Y-m-d h:i:s')."' ";

				if ($_POST['stancefrom'] != "--Any--" and $_POST['stanceto'] != "--Any--") {
					$Q_pre = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."' AND UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."' AND ".$Q_pre;
				}
				elseif ($_POST['stancefrom'] != "--Any--") {
					$Q_pre = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."' AND ".$Q_pre;
				}
				elseif ($_POST['stanceto'] != "--Any--") {
					$Q_pre = " UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."' AND ".$Q_pre;
				}
			}
			
			if ($Q_pre != '' and $Q_post != '') {
				$Q_stanceChange = " ((".$Q_pre.") OR (".$Q_post.")) ";
			}
			elseif ($Q_pre != '') {
				$Q_stanceChange = " (".$Q_pre.") ";
			}
			elseif ($Q_post != '') {
				$Q_stanceChange = " (".$Q_post.") ";
			}
			elseif ($Q_pre == '' and $Q_post == '') {
				if ($_POST['stancefrom'] != "--Any--" and $_POST['stanceto'] != "--Any--") {
					$Q_stanceChange = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."' AND UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."'";
				}
				elseif ($_POST['stancefrom'] != "--Any--") {
					$Q_stanceChange = " UPPER(kingdoms.lastStance) = '".strtoupper($_POST['stancefrom'])."'";
				}
				elseif ($_POST['stanceto'] != "--Any--") {
					$Q_stanceChange = " UPPER(kingdoms.stance) = '".strtoupper($_POST['stanceto'])."'";
				}
			}

			// ======== Kingdom Stance =======
			// ******   QUERY CALC   ******
			// ============================
			if ($_POST['minWar'] != "" and $_POST['maxWar'] != "") {
				$Q_warRangeKD = ' kingdoms.wars BETWEEN '.min($_POST['minWar'],$_POST['maxWar']).' AND '.max($_POST['minWar'],$_POST['maxWar']);
			}
			else if ($_POST['minWar'] != "") {
				$Q_warRangeKD = ' kingdoms.wars >= '.$_POST['minWar'];
			}
			else if ($_POST['maxWar'] != "") {
				$Q_warRangeKD = ' kingdoms.wars <= '.$_POST['maxWar'];
			}
			else{
				$Q_warRangeKD = '';
			}

			// === Kingdom Networth ===
			// ****** QUERY CALC ******
			// ========================
			if ($_POST['minkdnw'] != "" and $_POST['maxkdnw'] != "") {
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.min($_POST['minkdnw'],$_POST['maxkdnw']).' AND '.max($_POST['minkdnw'],$_POST['maxkdnw']);
			}
			else if ($_POST['kdNW'] != "" and $_POST['minkdnw'] != ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.min($_POST['minkdnw'],($_POST['kdNW']*1.30)).' AND '.max($_POST['minkdnw'],($_POST['kdNW']*1.30));
			}
			else if ($_POST['kdNW'] != "" and $_POST['maxkdnw'] != ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.min($_POST['maxkdnw'],($_POST['kdNW']*0.70)).' AND '.max($_POST['maxkdnw'],($_POST['kdNW']*0.70));
			}
			else if ($_POST['kdNW'] != ""){
				$Q_nwRangeKD = ' kingdoms.kdNetworth BETWEEN '.($_POST['kdNW']*0.70).' AND '.($_POST['kdNW']*1.30);
			}
			else{
				$Q_nwRangeKD = '';
			}

			// ===== Kingdom  Land =====
			// ******  QUERY CALC ******
			// =========================
			if ($_POST['minAc'] != "" and $_POST['maxAc'] != "") {
				$Q_landRangeKD = ' kingdoms.land BETWEEN '.min($_POST['minAc'],$_POST['maxAc']).' AND '.max($_POST['minAc'],$_POST['maxAc']);
			}
			else if ($_POST['minAc'] != "") {
				$Q_landRangeKD = ' kingdoms.land > '.$_POST['minAc'];
			}
			else if ($_POST['maxAc'] != "") {
				$Q_landRangeKD = ' kingdoms.land < '.$_POST['maxAc'];
			}
			else{
				$Q_landRangeKD = '';
			}

			// ===== Kingdom Honor =====
			// ******  QUERY CALC ******
			// =========================
			if ($_POST['minHon'] != "" and $_POST['maxHon'] != "") {
				$Q_honorRangeKD = ' kingdoms.honor BETWEEN '.min($_POST['minHon'],$_POST['maxHon']).' AND '.max($_POST['minHon'],$_POST['maxHon']);
			}
			else if ($_POST['minHon'] != "") {
				$Q_honorRangeKD = ' kingdoms.honor > '.$_POST['minHon'];
			}
			else if ($_POST['maxHon'] != "") {
				$Q_honorRangeKD = ' kingdoms.honor < '.$_POST['maxHon'];
			}
			else{
				$Q_honorRangeKD = '';
			}

			// ===== Kingdom PV Count =====
			// ******   QUERY CALC   ******
			// ============================
			if ($_POST['minPVnum'] != "" and $_POST['maxPVnum'] != "") {
				$Q_countRangeKD = ' kingdoms.count BETWEEN '.min($_POST['minPVnum'],$_POST['maxPVnum']).' AND '.max($_POST['minPVnum'],$_POST['maxPVnum']);
			}
			else if ($_POST['minPVnum'] != "") {
				$Q_countRangeKD = ' kingdoms.count > '.$_POST['minPVnum'];
			}
			else if ($_POST['maxPVnum'] != "") {
				$Q_countRangeKD = ' kingdoms.count < '.$_POST['maxPVnum'];
			}
			else{
				$Q_countRangeKD = '';
			}

			// ===== Kingdom Location =====
			// ******   QUERY CALC   ******
			// ============================
			if ($_POST['loc'] != "") {
				$Q_locPV = " kingdoms.loc = '".$_POST['loc']."'";
			}

			// ==== Kingdoms Name =====
			// ****** QUERY CALC ******
			// ========================
			if ($_POST['kdname'] != "") {
				$Q_namePV = " UPPER(kingdoms.name) LIKE '%".strtoupper($_POST['kdname'])."%'";
			}

			// ===== Kingdoms Island =====
			// ******   QUERY CALC  ******
			// ===========================
			$start = 1;
			$end = 8;
			$allIsl = 0;
			$queryArr = array();
			if ($_POST['minIsl'] != "" and $_POST['maxIsl'] != "") {
				$start = min((int)$_POST['minIsl'],(int)$_POST['maxIsl']);
				$end = max((int)$_POST['minIsl'],(int)$_POST['maxIsl']);
			}
			else if ($_POST['minIsl'] != "") {
				$start = (int)$_POST['minIsl'];
			}
			else if ($_POST['maxIsl'] != "") {
				$end = (int)$_POST['maxIsl'];
			}
			else{
				$allIsl = 1;
			}
			if ($allIsl != 1) {
				foreach (range($start, $end) as $num) {
					array_push($queryArr, " kingdoms.loc LIKE '".$num.":%'");
				}
				$Q_islPV = " (".join(' OR ',$queryArr).") ";
			}

			// ===== Kingdoms Race Count =====
			// ******    QUERY CALC     ******
			// ===============================
			$races = array('avian', 'dwarf', 'elf', 'faery', 'halfling', 'human', 'orc', 'undead', 'dryad');
			$raceFilters = array();
			foreach ($races as $r) {
				if ($_POST['min'.$r] != "" and $_POST['max'.$r] != "") {
					array_push($raceFilters, " 	kingdoms.loc IN ( SELECT k.loc FROM `kingdoms` k 
												INNER JOIN provinces p ON k.loc = p.loc
               									WHERE UPPER(p.race) = '".strtoupper($r)."'
												GROUP BY p.loc, p.race
               									HAVING COUNT(p.race) 
               									BETWEEN ".min($_POST['min'.$r],$_POST['max'.$r])."
               									AND ".max($_POST['min'.$r],$_POST['max'.$r])." )");
				}
				else if ($_POST['min'.$r] != "") {
					array_push($raceFilters, " 	kingdoms.loc IN ( SELECT k.loc FROM `kingdoms` k 
												INNER JOIN provinces p ON k.loc = p.loc
               									WHERE UPPER(p.race) = '".strtoupper($r)."'
												GROUP BY p.loc, p.race
               									HAVING COUNT(p.race) > ".$_POST['min'.$r]." )");
				}
				else if ($_POST['max'.$r] != "") {
					array_push($raceFilters, " 	kingdoms.loc IN ( SELECT k.loc FROM `kingdoms` k 
												INNER JOIN provinces p ON k.loc = p.loc
               									WHERE UPPER(p.race) = '".strtoupper($r)."'
												GROUP BY p.loc, p.race
               									HAVING COUNT(p.race) < ".$_POST['max'.$r]." )");
				}
				else{
					$Q_raceCount = '';
				}
			}
			if(!empty($raceFilters)){
				$Q_raceCount = join(" AND ",$raceFilters);
			}

			// ===== Building Final Query =====
			// ***    FILTER QUERY CALC     ***
			// ================================
			$Q_filter = " WHERE ";
			$Q_filter .= ($Q_nwRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_nwRangeKD
							:
								$Q_nwRangeKD
						:
							'';
			$Q_filter .= ($Q_landRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_landRangeKD
							:
								$Q_landRangeKD
						:
							'';
			$Q_filter .= ($Q_honorRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_honorRangeKD
							:
								$Q_honorRangeKD
						:
							'';
			$Q_filter .= ($Q_warwinRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_warwinRangeKD
							:
								$Q_warwinRangeKD
						:
							'';
			$Q_filter .= ($Q_warRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_warRangeKD
							:
								$Q_warRangeKD
						:
							'';
			$Q_filter .= ($Q_stanceChange != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_stanceChange
							:
								$Q_stanceChange
						:
							'';
			$Q_filter .= ($Q_countRangeKD != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_countRangeKD
							:
								$Q_countRangeKD
						:
							'';
			$Q_filter .= ($Q_locPV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_locPV
							:
								$Q_locPV
						:
							'';
			$Q_filter .= ($Q_namePV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_namePV
							:
								$Q_namePV
						:
							'';
			$Q_filter .= ($Q_islPV != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_islPV
							:
								$Q_islPV
						:
							'';
			$Q_filter .= ($Q_validStances != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_validStances
							:
								$Q_validStances
						:
							'';
			$Q_filter .= ($Q_ceasefires != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_ceasefires
							:
								$Q_ceasefires
						:
							'';
			$Q_filter .= ($Q_raceCount != '') ? 
							($Q_filter != " WHERE ") ?
								" AND ".$Q_raceCount
							:
								$Q_raceCount
						:
							'';

			$filterQuery = "SELECT 	kingdoms.name, 
									kingdoms.land, 
									kingdoms.loc, 
									kingdoms.honor, 
									kingdoms.stance, 
									kingdoms.warTarget, 
									kingdoms.warWin, 
									kingdoms.wars, 
									kingdoms.count, 
									ROUND((kingdoms.kdNetworth / kingdoms.land), 2) AS nwpa, 
									kingdoms.kdNetworth 
							FROM kingdoms ";
			
			$filterQuery .= ($Q_filter != " WHERE ")?
								$Q_filter
							:
								'';

			/*$filter = 0;
			if(isset($_POST['kdNW']) and $filter != 1 and $_POST['kdNW'] != ""){
				$lower = ($_POST['kdNW']*0.90);
				$upper = ($_POST['kdNW']*1.10);

				$filterQuery = $filterQuery." WHERE kingdoms.kdNetworth BETWEEN ".$lower." AND ".$upper;
			}*/
			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'loc'		=>	$row['loc'],
								'land'		=>	number_format($row['land']),
								'name'		=>	$row['name'],
								'wars'		=>	$row['wars'],
								'warWin'	=>	$row['warWin'],
								'stance'	=>	$row['stance'],
								'warTarget'	=>	$row['warTarget'],
								'nwpa'		=>	$row['nwpa'],
								'honor'		=>	number_format($row['honor']),
								'count'		=>	$row['count'],
								'kdNetworth'=>	number_format($row['kdNetworth']));
			}
		}
		$response['posts'] = $posts;
		$response['query'] = $Q_filter;
		$response['debugStr'] = $dbgstr;
		$response['queryExc'] = $filterQuery;
		echo(json_encode($response));
	}

	$conn->close();
?>