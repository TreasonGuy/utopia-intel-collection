<?php
	require('globals.php');

	function stripComma($int){
		if ($int == '-' || $int == '') {
			$int = 0;
		}
		$int = preg_replace('/[^0-9-\.]/', '', $int);
		return(doubleval($int));
	}

	function parseThrone($data){
		$SOT = array();
		global $MONTH;
		global $PERS;
		$arr = preg_split("/\n/",$data);
		$gu = 0;
		$rc = 0;
		$si = 0;
		$in = 0;
		$nw = 0;

		foreach ($arr as $line) {
			//echo("<br>$line,");
			if(preg_match('/^\s*Game Update\s*$/i', $line)){
				$gu = 1;
				$rc = 0;
				$si = 0;
				$in = 0;
				$nw = 0;
			}
			if(preg_match('/^\s*Royal commands\s*$/i', $line)){
				$gu = 0;
				$rc = 1;
				$si = 0;
				$in = 0;
				$nw = 0;
			}
			if(preg_match('/^\s*The Province of.*$/i', $line)){
				$gu = 0;
				$rc = 0;
				$si = 1;
				$in = 0;
				$nw = 0;
			}
			if(preg_match('/^\s*Info\s*$/i', $line)){
				$gu = 0;
				$rc = 0;
				$si = 0;
				$in = 1;
				$nw = 0;
			}
			if(preg_match('/^\s*Recent News\s*$/i', $line)){
				$gu = 0;
				$rc = 0;
				$si = 0;
				$in = 0;
				$nw = 1;
			}
			if ($rc == 1) {
				//echo "<br>$line,";
			}
			else if ($gu == 1) {
				//echo "<br>$line,";
			}
			else if ($in == 1) {
				if(preg_match('/^Spells\s*:(.*)$/', $line, $matches)){
					$data = preg_split('/\(|\)/', $matches[1]);
					for($i=0;$i<sizeof($data)-1;$i+=2){
						$SOT[trim($data[$i])] = stripComma(trim($data[$i+1]));
					}
				}
				else if(preg_match('/^Armies\s*:(.*)/', $line, $matches)){
					$data = preg_split('/ \(|\) \(|\) /', $matches[1]);
					$armies = array();
					for($i=1;$i<sizeof($data);$i+=2){
						preg_match('/[\d\.]+/', trim($data[$i]), $match);
						//echo "$match[0]";
						$armies[($i/2)+1] = array(	"gain" 		=> 	stripComma(trim($data[$i+1])),
													"return" 	=>	$match[0]);
					}
					$SOT['armies'] = $armies;
				}
			}
			else if ($nw == 1) {
				//echo "<br>$line,";
			}
			else if ($si == 1) {
				
				if(preg_match('/^The Province of ([\w\s]+) \(([\d:]+)/', $line, $matches)){
					$SOT['KD'] = $matches[2];
					$SOT['Province'] =  $matches[1];
				}
				else if(preg_match('/^(\w+) (\d+) of YR(\d+) \(next tick: (\d{0,2}) ?(\w+)\)/', $line, $matches)){
					$SOT['AGE'] = intval($matches[2]) + (24*($MONTH[$matches[1]]-1)*intval($matches[3]));
				}
				else if(preg_match('/^Race\s*(\w+)\s*Soldiers\s*([\d,]+)/', $line, $matches)){
					$SOT['Soldiers'] = stripComma($matches[2]);
					$SOT['Race'] =  $matches[1];
				}
				else if(preg_match('/^Ruler\s*(\w+)\s*([\w\s]+) the (\w+)\s+([\w\s]+)\s+([\d,]+)/', $line, $matches)){
					$SOT['Name'] = rtrim($matches[2]);
					$SOT['Honor'] =  $matches[1];
					$SOT['Persona'] =  $PERS[$matches[3]];
					$SOT['Ospecs'] =  stripComma($matches[5]);
				}
				else if(preg_match('/^Ruler\s*The (\w+) (\w+) ([\w\s]+)\s+([\w\s]+)\s+([\d,]+)/', $line, $matches)){
					$SOT['Name'] = rtrim($matches[3]);
					$SOT['Honor'] =  $matches[2];
					$SOT['Persona'] =  $PERS[$matches[1]];
					$SOT['Ospecs'] =  stripComma($matches[5]);
				}
				else if(preg_match('/^Land\s*([\d,]+)\s*([\w\s]+)\s+([\d,]+)/', $line, $matches)){
					$SOT['Land'] = stripComma($matches[1]);
					$SOT['Dspecs'] =  stripComma($matches[3]);
				}
				else if(preg_match('/^Peasants\s*([\d,]+)\s*([\w\s]+)\s+([\d,]+)/', $line, $matches)){
					$SOT['Peasants'] = stripComma($matches[1]);
					$SOT['Elites'] =  stripComma($matches[3]);
				}
				else if(preg_match('/^Building Eff\.\s*([\d\.]+)%\s*Thieves\s+([\d,]+)\s+\((\d+)%\)/', $line, $matches)){
					$SOT['BE'] = floatval($matches[1]);
					$SOT['Thieves'] =  stripComma($matches[2]);
					$SOT['Stealth'] =  floatval($matches[3]);
				}
				else if(preg_match('/^Money\s*([\d,]+)\s*Wizards\s+([\d,]+)\s+\((\d+)%\)/', $line, $matches)){
					$SOT['Money'] = stripComma($matches[1]);
					$SOT['Wizards'] =  stripComma($matches[2]);
					$SOT['Mana'] =  floatval($matches[3]);
				}
				else if(preg_match('/^Food\s*([\d,]+)\s*War Horses\s+([\d,]+)/', $line, $matches)){
					$SOT['Horses'] = stripComma($matches[2]);
					$SOT['Food'] =  stripComma($matches[1]);
				}
				else if(preg_match('/^Runes\s*([\d,]+)\s*Prisoners\s+([\d,]+)/', $line, $matches)){
					$SOT['Prisoners'] = stripComma($matches[2]);
					$SOT['Runes'] =  stripComma($matches[1]);
				}
				else if(preg_match('/^Trade Balance\s*([-\d,]+)\s*Off. Points\s+([\d,]+)/', $line, $matches)){
					$SOT['ThroneOff'] = stripComma($matches[2]);
					$SOT['TB'] =  stripComma($matches[1]);
					echo "$matches[1]";
				}
				else if(preg_match('/^Networth\s*([\d,]+) gold coins\s*Def. Points\s+([\d,]+)/', $line, $matches)){
					$SOT['ThroneDef'] = stripComma($matches[2]);
					$SOT['NW'] =  stripComma($matches[1]);
				}
				else if(preg_match('/^Our Kingdom is at WAR!\s*$/', $line, $matches)){
					$SOT['WAR'] =  true;
				}
				else if(preg_match('/^The Plague has spread throughout our people!\s*$/', $line, $matches)){
					$SOT['Plague'] =  true;
				}
				else if(preg_match('/^Riots due to housing shortages from overpopulation are hampering tax collection efforts!\s*$/', $line, $matches)){
					$SOT['Riot'] =  true;
				}
				else if(preg_match('/^We are covered by the (\w+) ritual with ([\d\.]+)% strength left!\s*$/', $line, $matches)){
					$SOT['Ritual'] = $matches[1];
					$SOT['RitualStrength'] =  floatval($matches[2]);
				}
				else{
					//echo "<br>ELSE:$line";
				}
			}
		}
		//print_r(json_encode($SOT));
	}

	function parseState($data){
		global $ALIAS;
		$STATE = array();
		if(preg_match('/^Affairs of the State\
\s*(\w+) ([\w\s]+), I track some important information about the health of our province. I hope you will find this information useful.\
\
Our Population\s+Our Economy\s+Current Highlights\
Peasants\s+(\d+)\s+Peasants\s+(\d+)\s+Current Networth\s+([\d,]+)\s+gold coins\
Army\s+(\d+)\s+Unemployed Peasants\s+(\d+)\s+Current Land\s+([\d,]+)\s+acres\
Thieves\s+(\d+)\s+Unfilled Jobs\s+(\d+)\s+Current Honor\s+([\d,]+)\
Wizards\s+(\d+)\s+Employment\s+([\d\.]+)%\s+Land Rank\s+(\d+) of (\d+)\
Total\s+(\d+)\s+Daily Income\s+(\d+)\s+Networth Rank\s+(\d+) of (\d+)\
Max Population\s+(\d+)\s+Daily Wages\s+(\d+)\s+\s+\
Recent Trends\
Net Change Yesterday\s+Net Change This Month\s+Net Change Last Month\
Our Income\s+([\d,]+)gc\s+([\d,]+)gc\s+([\d,]+)gc\
Military Wages\s+([\d,]+)gc\s+([\d,]+)gc\s+([\d,]+)gc\
Draft Costs\s+([\d,]+)gc\s+([\d,]+)gc\s+([\d,]+)gc\
Net Change\s+\
([\d,-]+)gc\
([\d,-]+)gc\
([\d,-]+)gc\
Peasants\s+\
([\d,-]+) peasants\
([\d,-]+) peasants\
([\d,-]+) peasants\
Food Grown\s+([\d,]+) bushels\s+([\d,]+) bushels\s+([\d,]+) bushels\
Food Needed\s+([\d,]+) bushels\s+([\d,]+) bushels\s+([\d,]+) bushels\
Food Decayed\s+([\d,]+) bushels\s+([\d,]+) bushels\s+([\d,]+) bushels\
Net Change\s+\
([\d,-]+) bushels\
([\d,-]+) bushels\
([\d,-]+) bushels\
Runes Produced\s+([\d,]+) runes\s+([\d,]+) runes\s+([\d,]+) runes\
Runes Decayed\s+([\d,]+) runes\s+([\d,]+) runes\s+([\d,]+) runes\
Net Change\s+\
([\d,-]+) runes\
([\d,-]+) runes\
([\d,-]+) runes/', $data, $matches));
		$STATE = array(	'Honor' 		=> $matches[1], 
						'Name' 			=> $matches[2],
						'Peasants' 		=> stripComma($matches[3]),
						'NW' 			=> stripComma($matches[5]),
						'Army' 			=> stripComma($matches[6]),
						'Unemployed' 	=> stripComma($matches[7]),
						'Land' 			=> stripComma($matches[8]),
						'Thieves' 		=> stripComma($matches[9]),
						'Vacancies' 	=> stripComma($matches[10]),
						'HonorVal' 		=> stripComma($matches[11]),
						'Wizards' 		=> stripComma($matches[12]),
						'Employment' 	=> floatval($matches[13]),
						'LRank' 		=> stripComma($matches[14]),
						'Total' 		=> stripComma($matches[15]),
						'TotalArmies' 	=> stripComma($matches[16]),
						'Income' 		=> stripComma($matches[17]),
						'NWRank' 		=> stripComma($matches[18]),
						'MaxPop' 		=> stripComma($matches[20]),
						'Wages' 		=> stripComma($matches[21]),
						'YestIncome' 	=> stripComma($matches[22]),
						'YestWage' 		=> stripComma($matches[25]),
						'YestDrCst' 	=> stripComma($matches[28]),
						'YestNet' 		=> stripComma($matches[31]),
						'PezGain' 		=> stripComma($matches[34]),
						'FGrown' 		=> stripComma($matches[37]),
						'FDecay' 		=> stripComma($matches[43]),
						'FNeeded' 		=> stripComma($matches[40]),
						'FNet' 			=> stripComma($matches[46]),
						'RProduced' 	=> stripComma($matches[49]),
						'RDecayed' 		=> stripComma($matches[52]),
						'RNet' 			=> stripComma($matches[55])
					  );

		# Obtaining other data
		preg_match('/Riots restrict our ability to collect taxes \(Estimated: (\d+) days more\)/', $data, $matches);
		$STATE['RiotLen'] = stripComma($matches[1]);
		//echo "<br>";
		//var_dump(preg_replace('/,|{|}/', '<br>', json_encode($STATE)));
	}

	function parseMilitaryCouncil($data){
		global $ALIAS;
		$STATE = array();
		echo preg_match('/^([\w\s]+), we have (\d) generals available to lead our armies. One must always stay here to lead our forces in defense, but you may send the others out to combat at any time.[\r\n]*
Our Generals\' current activities and our military strength is detailed below.[\r\n]*
Wage Rate[\r\n]*
Our military effectiveness is determined by the wage rates of our armed forces. Higher wages can drive up productivity. Any changes to pay will take time to change our efficiency.[\r\n]*
At this time, approximately ([\d\.]+)% of our maximum population is allocated to non-peasant roles. Our wage rate is ([\d\.]+)% of normal levels, and our military is functioning at ([\d\.]+)% efficiency.[\r\n]*
Military Strength[\r\n]*
In addition, our military strength is affected by a number of other factors including buildings, honor, spells, and more. Our net military effectiveness is as follows:[\r\n]*
Offensive Military Effectiveness\s*([\d\.]+)%\s*Net Offensive Points at Home\s*([\d\,]+)[\r\n]*
Defensive Military Effectiveness\s*([\d\.]+)%\s*Net Defensive Points at Home\s*([\d\,]+)[\r\n]*
Army Availability
(Standing Army[\w\W\n\r]*)
(Generals)\t([\d\t]+)
(Soldiers)\t([\d\t,]+)
([\w\S\']+)\t([\d\t,]+)
([\w\S\']+)\t([\d\t\-,]+)
([\w\S\']+)\t([\d\t,]+)
(War Horses)\t([\d\t,]+)
(Captured Land)\t([\d\t,\-]+)
Military Training Estimates
([\w\s]+), the table below shows our training estimates for the next 24 days.

1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24
([\w\S\']+)\t([\d\t,]+)
([\w\S\']+)\t([\d\t,]+)
([\w\S\']+)\t([\d\t,]+)
(Thieves)\t([\d\t,]+)
/', $data, $matches);
		$armies = array();
		$training = array();
		$numArmies = 0;
		preg_replace('/[\r\n]+/', '', $matches[10]);
		$arr = preg_split('/\t+/', $matches[10]);
		foreach ($arr as $key => $value) {
			if (preg_match('/Undeployed Army/', $value)) {continue;}
			else{
				$numArmies++;
				if (preg_match('/Standing Army/', $value)){$armies[0]['type'] = 'Armies Home';}
				else if (preg_match('/Army\s+#(\d)\s+\(([\d\.]+)\s+days?\s+left\)/', $value, $army)){
					$armies[intval($army[1])]['type'] = 'Army Away';
					$armies[intval($army[1])]['return'] = floatval($army[2]);
				}
			}
		}
		for ($i=12; $i <= 24; $i+=2) { 
			preg_replace('/[\r\n]+/', '', $matches[$i]);
			$arr = preg_split('/\t+|\s+/', $matches[$i]);
			for ($j=0; $j < $numArmies; $j++) { 
				$armies[$j][$ALIAS[strtolower($matches[$i-1])]] = intval(stripComma($arr[$j]));
			}
		}
		for ($i=27; $i <= 33; $i+=2) { 
			preg_replace('/[\r\n]+/', '', $matches[$i]);
			$arr = preg_split('/\t|\s+/', $matches[$i]);
			$sum = 0;
			for ($j=0; $j < 24; $j++) { 
				$training[$ALIAS[strtolower($matches[$i-1])]][$j] = intval(stripComma($arr[$j]));
				$sum += $training[$ALIAS[strtolower($matches[$i-1])]][$j];
			}
			$training[$ALIAS[strtolower($matches[$i-1])]."total"] = $sum;
		}
		$MILITARY = array(	'PezPrcnt' 		=> 100.00 - floatval($matches[3]), 
							'GenHome'		=> $matches[2],
							'WageRate' 		=> floatval($matches[3]),
							'ME' 			=> floatval($matches[5]),
							'OME' 			=> floatval($matches[6]),
							'DME'	 		=> floatval($matches[8]),
							'OffHome'		=> stripComma($matches[7]),
							'DefHome' 		=> stripComma($matches[9]),
							'Armies'	 	=> $armies,
							'Training' 		=> $training
					  );

		//echo "<br>";
		//var_dump(preg_replace('/,|{|}/', '<br>', json_encode($MILITARY)));
		//var_dump(preg_replace('/,|{|}/', '<br>', json_encode($matches)));
	}

	function parseBuildingCouncil($data){
		global $ALIAS;
		$BUILD = array(	'Available Workers' 	=> 0,
						'Building Efficiency'	=> 0,
						'Available Jobs'		=> 0,
						'Barren Land'			=> 0,
						'Homes'					=> 0,
						'Farms'					=> 0,
						'Mills'					=> 0,
						'Banks'					=> 0,
						'Training Grounds'		=> 0,
						'Armouries'				=> 0,
						'Military Barracks'		=> 0,
						'Forts'					=> 0,
						'Guard Stations'		=> 0,
						'Hospitals'				=> 0,
						'Guilds'				=> 0,
						'Towers'				=> 0,
						'Thieves\' Dens'		=> 0,
						'Watch Towers'			=> 0,
						'Laboratories'			=> 0,
						'Universities'			=> 0,
						'Stables'				=> 0,
						'Dungeons'				=> 0,
						'Towers'				=> 0,
						'Towers'				=> 0,
						);
		foreach ($BUILD as $key => $value) {
			if(preg_match("/$key(?:\t|\s+)([\d\,]+)(?:\t|\s+)([\d\.]+)/", $data, $match)){
				$BUILD[$key] = array(	'count' => intval($match[1]),
										'ratio' => floatval(stripComma($match[2])) );
			}
			elseif (preg_match("/$key(?:\t|\s+)([\d\,\.]+)/", $data, $match)) {
				$BUILD[$key] = floatval(stripComma($match[1]));
			}

			if (preg_match("/$key(\t|[\d\,]+){24}/", $data, $match)) {
				$cons = preg_split("/\t|\s/", $match[0]);
				//$cons = explode('\t', $match[1]);
				$sum = 0;
				foreach ($cons as $k => $val) {
					$cons[$k] = intval(stripComma($val));
					$sum += $cons[$k];
				}
				$BUILD[$key]['constr'] = $cons;
				$BUILD[$key]['incoming'] = $sum;
			}
		}

			//echo "<br>";
			//var_dump(preg_replace('/,|{|}/', '<br>', json_encode($BUILD)));
	}

	function parseScienceCouncil($data){
		global $ALIAS;
		$SCIENCE = array(	'Alchemy' 		=> 0,
						'Tools'			=> 0,
						'Housing'		=> 0,
						'Production'	=> 0,
						'Military'		=> 0,
						'Crime'			=> 0,
						'Channeling'	=> 0,
						);
		$sum = 0;
		foreach ($SCIENCE as $key => $value) {
			if(preg_match("/$key(?:\t|\s+)([\d\,]+)(?:\t|\s+)([\d\.]+)/", $data, $match)){
				$SCIENCE[$key] = array(	'count' => intval($match[1]),
										'ratio' => floatval(stripComma($match[2])) );
				$sum += intval($match[1]);
			}
		}
		$SCIENCE['total'] = $sum;

		//echo "<br>";
		//var_dump(preg_replace('/,|{|}/', '<br>', json_encode($SCIENCE)));
	}

?>
