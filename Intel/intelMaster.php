<?php
	# These headers must be present or else user will get errors.
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');

	require('parseFunctions.php');

	//if ($_POST) {
		/* UNCOMMENT WHEN LIVE
	    # Raw content of game div
	    $data_html = $_POST['data_html'];
	    # Content from game div as if user had copy pasted (useful for making parsers that also work with copy/paste)
	    $data_simple = $_POST['data_simple'];
	    # Url from where data came from (useful for parsing)
	    $url = $_POST['url'];
	    # Province that sent intel (will be different when sitting)
	    $province = $_POST['prov'];
	    # Key (You should give the kingdom, if not each user a personal key so you can restrict access)
	    $key = $_POST['key'];
	    $example_array = array($url,$province,$key);
	    */

	    require('../../Utopia/Sample_POSTDATA/Science.txt');
	    $example_array = array($url,$province,$key);
	    
	    # Calling parse functions based on url
	    if (preg_match("/^http:\/\/utopia-game.com\/wol\/game\/throne/", $url)) {
	    	echo "<br>Throne page detected<br>";
	    	print_r($example_array);
	    	parseThrone($data_simple);
	    }
	    else if (preg_match("/^http:\/\/utopia-game.com\/wol\/game\/council_state/", $url)) {
	    	echo "\nState page detected\n";
	    	parseState($data_simple);
	    }
	    else if (preg_match("/^http:\/\/utopia-game.com\/wol\/game\/council_military/", $url)) {
	    	echo "\nMilitary page detected\n";
	    	parseMilitaryCouncil($data_simple);
	    }
	    else if (preg_match("/^http:\/\/utopia-game.com\/wol\/game\/council_internal/", $url)) {
	    	echo "\nBuilding page detected\n";
	    	parseBuildingCouncil($data_simple);
	    }
	    else if (preg_match("/^http:\/\/utopia-game.com\/wol\/game\/council_science/", $url)) {
	    	echo "\nBuilding page detected\n";
	    	parseScienceCouncil($data_simple);
	    }

	    # Checking response

	    # Return json object with first instance being success: True
	    # If you do not do this, user will get an error. 
	    echo json_encode(array('success'=>true));
	    error_log("$data_html,$data_simple,$url,$province,$key", 3, "/storage/ssd1/487/1179487/public_html/PostREQ.log");
	//}
?>