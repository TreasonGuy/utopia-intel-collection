<?php
$MONTH = array(	'January' 	=> 1, 
				'February' 	=> 2, 
				'March' 	=> 3, 
				'April' 	=> 4, 
				'May' 		=> 5, 
				'June' 		=> 6, 
				'July' 		=> 7);

$PERS = array(	'Blessed'	=> 'Cleric',
				'Wise'		=> 'Sage',
				'Skeptic'	=> 'Heretic',
				'Sorcerer'	=> 'Mystic',
				'Sorceress'	=> 'Mystic',
				'Rogue'		=> 'Rogue',
				'Warrior'	=> 'Warrior',
				'Conniving'	=> 'Tactician',
				'Hero'		=> 'WarHero',
				'Chivalrous'=> 'Paladin',
				);

$ALIAS = array('building efficiency' => 'be', 'be' => 'be',

	'available workers' => 'peasants', 'peasants' => 'peasants', 'pez' => 'peasants',

	'land' => 'land', 'acre' => 'land', 'acres' => 'land', 'lands' => 'land', 'captured land' => 'land', 

	'solds' => 'soldiers', 'sold' => 'soldiers', 'sol' => 'soldiers', 'sols' => 'soldiers', 'soldier' => 'soldiers', 'soldiers' => 'soldiers',

	'gens' => 'general', 'gen' => 'general', 'generals' => 'general', 'general' => 'general',

	'thieves' => 'thieves', 'thief' => 'thieves',

	'horses' => 'horses', 'horse' => 'horses', 'war horses' => 'horses', 'war horses' => 'horses', 

	'griffins' => 'ospecs', 'huldras' => 'ospecs', 'rangers' => 'ospecs', 'magicians' => 'ospecs', 'strongarms' => 'ospecs', 'swordsmen' => 'ospecs', 'goblins' => 'ospecs', 'skeletons' => 'ospecs', 'warriors' => 'ospecs',
	
	'harpies' => 'dspecs', 'nymphs' => 'dspecs', 'axemen' => 'dspecs', 'archers' => 'dspecs', 'druids' => 'dspecs', 'slingers' => 'dspecs', 'archers' => 'dspecs', 'trolls' => 'dspecs', 'zombies' => 'dspecs', 
	
	'drakes' => 'elites', 'will o the wisps' => 'elites', 'berserkers' => 'elites', 'elf lords' => 'elites', 'beastmasters' => 'elites', 'brutes' => 'elites', 'knights' => 'elites', 'ogres' => 'elites', 'ghouls' => 'elites',);
?>