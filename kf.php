	<!DOCTYPE html>
	<html>
		<head>
			<?php 
				require('template/head.php');
			?>
			<style type="text/css">

				body {
				  /* Margin bottom by footer height */
				  margin-bottom: 60px;
				  background: url('/Utopia/img/globe.jpeg') no-repeat center center fixed;
				  -webkit-background-size: cover;
				  -moz-background-size: cover;
				  -o-background-size: cover;
				  background-size: cover;
				  color:#fff;
				  background-color:#eee;
				  font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
				  overflow-y:scroll;
				}

				.form-group{
					padding: 5px;
				}

				.input-group-addon {
					min-width: 95px;
					text-align: left;
				}

				.navbar-nav > li{
					min-width: 90px;
					text-align: center;
				}

				#advanced{
					border-top: 1px solid #888;
					padding: 0px;
					margin: 0px;
				}

				.options{
					min-width: 100px;
				}

				.smaller{
					max-width: 100px;
					max-height: 22px;
					padding: 5px;
					margin: 0;
				}

				.dropdown{
					padding: 0px;
					margin: 0;
					height: 25px;
					max-width: 200px;
					min-width: 100px;
					color: gray;
					border-radius: 4px;
				}

				.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
				  background-color: #111;
				}

				.table-hover tbody tr td, .table-hover tbody tr th {
				  max-width: 280px;
				}

				.btn-link, .btn-link:hover, .btn-link:active, .btn-link:focus{
					color: white;
					border: 1px solid white;
					border-radius: 4px;
				}

			</style>
		</head>

		<body id="homearea">
			<div class="showOnLoad" >
			
			<?php
				require('template/navigation.php');
			?>

			<section class="container-fluid" style="margin-top:50px; background-color: rgba(55,55,55,0.5);
					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.19);">
				<article style="opacity: none;">
					<h3 style="color: #EEEEEE;">Simplify your kingdom search:</h3>
					<form action="/Utopia/db2json.php" id="provinceFinder" method="post">	
						<div class="form-group col-sm-10">
							<input class="form-control" type="number" name="kdNW" placeholder="KD networth here" />
						</div>
						<div class="form-group col-sm-1">
							<input type="submit" id="goButton" class="btn btn-danger btn-block" value="GO!" />
						</div>
						<div class="form-group col-sm-1">
							<button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#advanced" type="button">
								<i class="glyphicon glyphicon glyphicon-filter"></i>
							</button>
						</div>
						<input name="action" type="hidden" value="kingdoms" />
						<div class="col-sm-12 collapse" id="advanced" style="margin: 5px;">
							<div class="col-sm-12">
								<h4>Basic Controls:</h4>
								<div class="col-sm-3">
									<label for="ceasefire">Ceasefires:</label><br/>
										<textarea name="ceasefire" placeholder="Paste your ceasefire page." 
										rows="4" style="color: black; resize: none; width: 90%;"></textarea>
								</div>
								<div class="col-sm-3">
									<label for="stance">Stance: <br/>
									<div class="btn-group">
									  <button type="button" class="btn btn-link btn-xs checkAllStance">Select All</button>
									  <button type="button" class="btn btn-link btn-xs uncheckAllStance">Deselect All</button>
									</div>
									</label><br/>
										<label class="options">
										<input class="stance" type="checkbox" name="normal" /> Normal</label>
										<label class="options">
										<input class="stance" type="checkbox" name="war" value="War" /> War</label>
										<label class="options">
										<input class="stance" type="checkbox" name="eowcf" value="eowcf" /> EoWCF</label>
										<label class="options">
										<input class="stance" type="checkbox" name="fort" value="Fort" /> Fort</label>
										<label class="options">
										<input class="stance" type="checkbox" name="aggro" value="Aggressive" /> Aggressive</label>
								</div>
								<div class="col-sm-3">
									<label for="stance">Micromanaging:</label><br/>
										<div class="input-group input-group-sm">
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minAc" placeholder="Min Acre" />
												<input class="form-control smaller" type="number" name="maxAc" placeholder="Max Acre" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minHon" placeholder="Max honor" />
												<input class="form-control smaller" type="number" name="maxHon" placeholder="Min honor" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minkdnw" placeholder="Min NW" />
												<input class="form-control smaller" type="number" name="maxkdnw" placeholder="Max NW" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minPVnum" placeholder="Min #prov" />
												<input class="form-control smaller" type="number" name="maxPVnum" placeholder="Max #prov" />
											</div>
									</div>
								</div>
								<div class="col-sm-3">
									<label for="stance">More Micromanaging:</label><br/>
										<div class="input-group input-group-sm">
											<div class="form-group">
												<input class="form-control smaller" type="text" name="minIsl" placeholder="From island" />
												<input class="form-control smaller" type="text" name="maxIsl" placeholder="To island" />
											</div>
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minWW" placeholder="Min War Win" />
												<input class="form-control smaller" type="number" name="maxWW" placeholder="Max War Win" />
											</div>	
											<div class="form-group">
												<input class="form-control smaller" type="number" name="minWar" placeholder="Min Wars" />
												<input class="form-control smaller" type="number" name="maxWar" placeholder="Max Wars" />
											</div>	
											<div class="form-group">
												<input class="form-control smaller" type="text" name="loc" placeholder="Location" />
												<input class="form-control smaller" type="text" name="kdname" placeholder="KD name" />
											</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<h4>Advanced controls :</h4>
								<div class="col-sm-3" >
									<label style="margin:0; padding:0;">Stance modifiers:</label>
									<div class="form-group" style="margin:0; padding:0 0 0 10px;">
										<label for="stancefrom" style="margin:0; padding:0;">From:</label>
										<select class="form-control dropdown" id="stancefrom">
											<option>--Any--</option>
											<option>Aggressive</option>
											<option>EoWCF</option>
											<option>Fortified</option>
											<option>Normal</option>
											<option>War</option>
										</select>
										<label for="stanceto" style="margin:0; padding:0;">To:</label>
										<select class="form-control dropdown" id="stanceto">
											<option>--Any--</option>
											<option>Aggressive</option>
											<option>EoWCF</option>
											<option>Fortified</option>
											<option>Normal</option>
											<option>War</option>
										</select>
									</div>
									<div class="input-group input-group-sm" style="padding-bottom: 20px;">
										<label class="options" style="margin:10px 0 0px 0;">Tick Range :</label>
										<div class="form-group" style="margin:0; padding:0 0 0 10px;">
											<input class="form-control smaller" type="number" name="hrAgo" placeholder="-hr" min="0" step="1" />
											<input class="form-control smaller" type="number" name="hrAhead" placeholder="+hr" min="0" step="1" />
										</div>
									</div>
								</div>
								<div class="col-sm-3" >
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Avian :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="minavian" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxavian" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Dwarf :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="mindwarf" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxdwarf" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Elf :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="minelf" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxelf" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
								</div>
								<div class="col-sm-3" >
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Faery :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="minfaery" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxfaery" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Human :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="minhuman" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxhuman" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Halfer :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="minhalfer" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxhalfer" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
								</div>
								<div class="col-sm-3" >
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Orc :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="minorc" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxorc" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Undead :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="minundead" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxundead" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
									<div class="input-group input-group-sm">
										<label class="options" style="margin:10px 0 0px 0;">Dryad :</label>
										<div class="form-group">
											<input class="race form-control smaller" type="number" name="mindryad" placeholder="Min"  min="0" step="1" />
											<input class="race form-control smaller" type="number" name="maxdryad" placeholder="Max"  min="0" step="1" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</article>
			</section>

			<div class="container table-responsive shadowBG" id="searchResult">
				<?php
					require('template/alerts.php');
				?>
			</div>

			<?php
				require('template/footer.php');
			?>
			</div>

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="/Utopia/js/__jquery.tablesorter/jquery.tablesorter.js"></script>					
			<script src='/Utopia/js/kf.js'></script>

			<script type="text/javascript">
			
			</script>

		</body>
	</html>