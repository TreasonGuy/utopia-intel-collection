<?php
	require('db_conn.php');

	if(isset($_POST['action'])){
		if($_POST['action'] == 'racechart'){
			$filterQuery = "SELECT COUNT(p.name) as count , p.race FROM provinces p GROUP BY p.race";

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'y'		=>	$row['count'],
								'name'	=>	$row['race']);
			}
		}
		else if($_POST['action'] == 'distribution'){
			if(isset($_POST['axis'])){
				if($_POST['axis'] == 'kdhon'){
					$col = 'k.honor as axis';
				}
				else if($_POST['axis'] == 'kdland'){
					$col = 'k.land as axis';
				}
				else if($_POST['axis'] == 'kdww'){
					$col = '(k.warWin/k.wars) as axis';
				}
				else{
					$col = 'k.kdNetworth as axis';
				}
			}
			else{
				$col = 'k.kdNetworth as axis';
			}

			$filterQuery = "SELECT (COUNT(p.name)/k.count) as ratio, ".$col." 
							FROM provinces p 
							INNER JOIN kingdoms k ON p.loc = k.loc 
							WHERE p.race = '".$_POST['race']."' 
							GROUP BY p.loc, p.race
							ORDER BY axis ASC";

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'y'		=>	$row['ratio'],
								'name'	=>	$row['axis']);
			}
		}
		else if($_POST['action'] == 'honorchart'){
			$filterQuery = "SELECT COUNT(p.name) as count , p.honor FROM provinces p GROUP BY p.honor";

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'y'		=>	$row['count'],
								'name'	=>	$row['honor']);
			}
		}
		else if($_POST['action'] == 'honordist'){
			$filterQuery = "SELECT p.race, (COUNT(p.race)/T.count) ratio, COUNT(p.race) as count, T.count as total
							FROM   (SELECT COUNT(name) AS count, race 
									FROM provinces 
									GROUP BY race) AS T, provinces p 
							WHERE p.honor = '".$_POST['honor']."' 
								AND p.race = T.race GROUP BY p.race";

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'y'		=>	$row['ratio'],
								'x'		=>	$row['count'],
								'total'	=>	$row['total'],
								'name'	=>	$row['race']);
			}
		}
		else if($_POST['action'] == 'homepage'){
			$filterQuery = "SELECT 	(SELECT COUNT(*) FROM kingdoms) as kd, 
									(SELECT COUNT(*) FROM provinces) as pv, 
									(SELECT COUNT(*) 
									 FROM kingdoms 
									 WHERE stance = 'Fortified') as fort, 
									(SELECT COUNT(*) 
									 FROM kingdoms 
									 WHERE stance = 'Normal') as normal, 
									(SELECT COUNT(*) 
									 FROM kingdoms 
									 WHERE stance = 'Aggressive') as aggro, 
									(SELECT COUNT(*) 
									 FROM kingdoms 
									 WHERE stance = 'war') as war, 
									(SELECT COUNT(*) FROM provinces) as pv, 
									updated
							FROM provinces";

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'kingdoms'		=>	$row['kd'],
								'provinces'		=>	$row['pv'],
								'fort'			=>	$row['fort'],
								'normal'		=>	$row['normal'],
								'war'			=>	$row['war'],
								'aggro'			=>	$row['aggro'],
								'updated'		=>	$row['updated']);
			}
		}
		else if($_POST['action'] == 'treemapKD'){
			$filter = '';

			if(isset($_POST['location']) && isset($_POST['select'])){
				if($_POST['location'] != "" && preg_match("/^\d{1,2}\:\d{1,2}$/", $_POST['location'])){
					$filter = " WHERE loc = '" . $_POST['location'] . "'";
				}
				if($_POST['select'] == 'provinces'){
					$col = 'race as col, name, ';
				}
				else{
					$col = 'loc as name, name as col, ';
					if ($_POST['axis'] == 'nw') {
						$_POST['axis'] = 'kdNetworth';
					}
				}
			}
			
			$filterQuery = "SELECT " . $col  . $_POST['axis'] . " as axis FROM " . $_POST['select'] . $filter;

			$response = array();
			$posts = array();
			$result = mysqli_query($conn, $filterQuery);
			while($row=mysqli_fetch_array($result)){
				$posts[] =	array(
								'col'	=>	$row['col'],
								'name'	=>	$row['name'],
								'axis'	=>	$row['axis']);
			}
		}

		$response['posts'] = $posts;
		$response['query'] = $filterQuery;
		echo(json_encode($response));
	}

	$conn->close();
?>